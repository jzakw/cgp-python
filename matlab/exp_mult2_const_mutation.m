experiments = {{ "exp\mult2_grid_size", "Multiplicator2bit_5x5", "Goldman"}
    { "exp\mult2_const_mutation", "Multiplicator2bit_5x5_mr_0.01", "0.01"}
    { "exp\mult2_const_mutation", "Multiplicator2bit_5x5_mr_0.02", "0.02"}
    { "exp\mult2_const_mutation", "Multiplicator2bit_5x5_mr_0.03", "0.03"}
    { "exp\mult2_const_mutation", "Multiplicator2bit_5x5_mr_0.1", "0.1"}
    { "exp\mult2_const_mutation", "Multiplicator2bit_5x5_mr_0.2", "0.2"}
    { "exp\mult2_const_mutation", "Multiplicator2bit_5x5_mr_0.3", "0.3"}
    };

experiments_data = load_data(experiments);
%close all;
take = 0.5;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.functionality(k) == 1 && table.active_nodes(k) == 7 && table.longest_path(k) == 2;
[idx, max_gen, max_node_evals, fig1, fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "Mutation rate");
fig1.Position = [10 10 800 500];
fig2.Position = [10 10 800 500];
print(fig1, '-dpdf', 'const_mutation_gens.pdf');
print(fig2, '-dpdf', 'const_mutation_nodeevals.pdf');
fig1.Position = [10 10 500 350];
fig2.Position = [10 10 500 350];
print(fig1, '-dpdf', 'const_mutation_gens_small.pdf');
print(fig2, '-dpdf', 'const_mutation_nodeevals_small.pdf');

%%
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fun_process = @(x, dim)mean(x, dim); %quantile(x,0.5, dim);
fig3 = plot_data_progress(experiments, experiments_data, take, idx, max_gen, fun_cmp, fun_val, fun_process, "Generations", 1000);
title("Průměrné fitness (top "+ ceil(take*100) +" % z 50 běhů)");

fig3.CurrentAxes.YLim = [320.1 344.9];
fig3.Position = [10 10 800 500];
legend([],'Position',[0.7 0.2 0.1 0.2]);
print(fig3, '-dpdf', 'const_mutation_plot.pdf');