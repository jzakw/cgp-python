function [result] = transform_to_csv(data)
    
    [rows, cols] = size(data);
    result = zeros(rows * cols, 2);

    k = 1;
    for j = 1:cols
        for i = 1:rows
            result(k, 1) = data(i, j);
            result(k, 2) = j;
            k = k + 1;
        end
    end
end