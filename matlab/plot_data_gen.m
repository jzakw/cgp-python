function [fig] = plot_data_gen(experiments, experiments_data, take, idx, gen, fun_cmp, fun_val, value_label, experiment_max)
    lengths = zeros(1, length(experiments_data));
    for i = 1:length(experiments_data)
        lengths(i) = length(experiments_data{i});
    end
    exps_cnt = min(lengths);
    exps_top_cnt = ceil(exps_cnt*take);
    

    labels = strings(1, length(experiments_data));
    for i = 1:length(experiments_data)
        labels(i) = experiments{i}{3};
    end

    data_x =  [gen];
    data_y = zeros([length(experiments_data), exps_cnt, length(data_x)]);

    for i = 1:length(experiments_data)
        for j = 1:exps_cnt
            table = experiments_data{i}{j};
            k = 1;
            l = 1;
            while k <= length(table.generation) && l <= length(data_x)
                if fun_cmp(table, k, i) >= data_x(l)
                    data_y(i, j, l) = fun_val(table, k, i);
                    l = l+1;
                end
                k = k+1;
            end
    
            k = k-1;
            while l <= length(data_x)
                data_y(i, j, l) = fun_val(table, k, i);
                l = l+1;
            end
        end
    end

    data_y_sorted = zeros(size(data_y));
    for i=1:size(data_y,1)
        data_y_sorted(i, :, :) = data_y(i, idx(i, :), :);
    end

    data_y_sorted_halve = data_y_sorted(:, 1:exps_top_cnt, :);
   
    fig = figure('Renderer', 'painters', 'Position', [10 10 900 600]);
    tiledlayout(fig, 1, 1, 'padding', 'tight');

    hold on    
    boxplot(data_y_sorted_halve', 'Labels', labels);
    set(gca,'TickLabelInterpreter','latex');
    xlabel(value_label);
    ylabel('Fitness');
    title("Fitness pro "+ gen +". generaci (top "+ ceil(take*100) +" % z "+ exps_cnt +" běhů)")
    fig.PaperSize = fig.PaperPosition(3:4);

    if experiment_max > 0
        plot([0, size(data_y_sorted_halve, 1)+1], [experiment_max, experiment_max], 'k--');   
        ax = gca;
        fitness_min = min(data_y_sorted_halve, [], "all");
        ax.YLim = [fitness_min*0.98 experiment_max*1.02];
        ax.XLim = [0 size(data_y_sorted_halve, 1)+1];
    end
end