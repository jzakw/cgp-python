function [fig] = plot_data_progress(experiments, experiments_data, take, idx, top, fun_cmp, fun_val, fun_process, value_label, elements)
    lengths = zeros(1, length(experiments_data));
    for i = 1:length(experiments_data)
        lengths(i) = length(experiments_data{i});
    end
    exps_cnt = min(lengths);
    exps_top_cnt = ceil(exps_cnt*take);
    

    labels = strings(1, length(experiments_data));
    for i = 1:length(experiments_data)
        labels(i) = experiments{i}{3};
    end

    data_x = ceil(linspace(1, top, elements));
    data_y = zeros([length(experiments_data), exps_cnt, length(data_x)]);

    for i = 1:length(experiments_data)
        for j = 1:exps_cnt
            table = experiments_data{i}{j};
            k = 1;
            l = 1;
            while k <= length(table.generation) && l <= length(data_x)
                if fun_cmp(table, k, i) >= data_x(l)
                    data_y(i, j, l) = fun_val(table, k, i);
                    l = l+1;
                end
                k = k+1;
            end
    
            k = k-1;
            while l <= length(data_x)
                data_y(i, j, l) = fun_val(table, k, i);
                l = l+1;
            end
        end
    end

    data_y_sorted = zeros(size(data_y));
    for i=1:size(data_y,1)
        data_y_sorted(i, :, :) = data_y(i, idx(i, :), :);
    end

    data_y_sorted_halve = data_y_sorted(:, 1:exps_top_cnt, :);

    experiment_max = max(data_y_sorted_halve, [], "all");
   



    fig = figure('Renderer', 'painters', 'Position', [10 10 900 600]);
    tiledlayout(fig, 1, 1, 'padding', 'tight');

    hold on

    processed_data = fun_process(data_y_sorted_halve, [2]);

    reshaped_data = zeros([length(experiments_data)*size(processed_data, 2), length(data_x)]);
    for i = 1:length(experiments_data)
        for j = 1:size(processed_data, 2)
            reshaped_data((j-1)*length(experiments_data)+i, :) = processed_data(i, j, :);
        end
    end

    oldcolors = get(gca,'colororder');
    newcolors = repmat(oldcolors(1:length(experiments_data), :), [3 1]);
    colororder(newcolors);

    data_y_to_plot = reshaped_data;
    
    for i = 1:size(data_y_to_plot, 1)
        if i<=length(experiments_data)
            plot(data_x, data_y_to_plot(i, :));
        else
            plot(data_x, data_y_to_plot(i, :), ':');
        end
    end

    xlabel(value_label);
    ylabel('Fitness');

    plot([min(data_x), max(data_x)], [experiment_max, experiment_max], 'k--');

    legend(labels, 'Interpreter','latex');

    title("Fitness (top "+ ceil(take*100) +" % z "+ exps_cnt +" běhů)");
    fig.PaperSize = fig.PaperPosition(3:4);
    
end