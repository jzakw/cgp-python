experiments = {
    { "exp\mult2_grid_size", "Multiplicator2bit_5x5", "1"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost", "5"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost2", "9"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost4", "17"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost8", "33"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost16", "65"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost32", "129"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost64", "257"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost128", "513"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost256", "1025"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost512", "2049"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost4096", "16385"}
    { "exp\mult2_startup_boost", "Multiplicator2bit_boost32768", "131073"}
    };

experiments_data = load_data(experiments);
disp("Data loaded.");
%close all;
take = 0.5;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.functionality(k) == 1 && table.active_nodes(k) == 7 && table.longest_path(k) == 2;
[idx, max_gen, max_node_evals, fig1, fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "Initial random set size");

print(fig1, '-dpdf', 'startup_boost_gens.pdf');
%print(fig2, '-dpdf', 'grid_nodeevals.pdf');

%% Gen 0
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fig3 = plot_data_gen(experiments, experiments_data, take, idx, 0, fun_cmp, fun_val, "Initial random set size", 343);
fig3.Position = [10 10 500 350];
print(fig3, '-dpdf', 'startup_boost_gen0.pdf');

%% Gen 1000
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fig3 = plot_data_gen(experiments, experiments_data, take, idx, 1000, fun_cmp, fun_val, "Initial random set size", 343);
fig3.Position = [10 10 500 350];
print(fig3, '-dpdf', 'startup_boost_gen1000.pdf');