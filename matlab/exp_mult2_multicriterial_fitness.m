experiments = {{ "exp\mult2_grid_size", "Multiplicator2bit_5x5", "Single-step"}
    { "exp\mult2_multicriterial_fitness", "Multiplicator2bit_5x5_twostep", "Two-step"}
    };

experiments_data = load_data(experiments);
%close all;
take = 0.5;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.functionality(k) == 1 && table.active_nodes(k) == 7 && table.longest_path(k) == 2;
[idx, max_gen, max_node_evals, fig1, fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "Fitness function");

fig1.Position = [10 10 600 400];
print(fig1, '-dpdf', 'fitness_gens.pdf');
%print(fig2, '-dpdf', 'const_mutation_nodeevals.pdf');
close(fig2);

%%
fun_cmp = @(table, k, i)table.generation(k);
fun_vals = {
    @(table, k) table.fitness(k)
    @(table, k) table.fitness(k)*5+(table.functionality(k)==1)*2+(table.functionality(k)~=1)*(25 - table.active_nodes(k))+(table.functionality(k)~=1)*(5 - table.longest_path(k))
};
fun_val = @(table, k, i)fun_vals{i}(table, k);
fun_process = @(x, dim)[quantile(x,0.5, dim), quantile(x,0.25, dim), quantile(x,0.75,dim)]; %quantile(x,0.5, dim);
fig3 = plot_data_progress(experiments, experiments_data, take, idx, max_gen, fun_cmp, fun_val, fun_process, "Generations", 10000);
title("Mod. fitness podle fit1 (top "+ ceil(take*100) +" % z 50 běhů)");

fig3.CurrentAxes.XLim = [0 2000];
fig3.CurrentAxes.YLim = [300 344.9];
fig3.Position = [10 10 500 350];
ylabel('Mod. fitness')

legend({"Single-step $Q_2$", "Two-step $Q_2$", "Single-step $Q_1, Q_3$", "Two-step $Q_1, Q_3$","", "", "Max fitness"}, 'Position',[0.7 0.2 0.1 0.2], 'Interpreter','latex');

print(fig3, '-dpdf', 'fitness_plot_2000_fit1.pdf');

%%
fun_vals = {
    @(table, k) (table.fitness(k)-(table.functionality(k)==1)*2-(table.functionality(k)~=1)*(25 - table.active_nodes(k))-(table.functionality(k)~=1)*(5 - table.longest_path(k)))/5
    @(table, k) table.fitness(k)
};
fun_val = @(table, k, i)fun_vals{i}(table, k);
fig4 = plot_data_progress(experiments, experiments_data, take, idx, max_gen, fun_cmp, fun_val, fun_process, "Generations", 10000);
title("Mod. fitness podle fit2 (top "+ ceil(take*100) +" % z 50 běhů)");

plot([0, 2000], [64 64], '--', 'color', [.5 .5 .5]);

fig4.CurrentAxes.YLim = [56 68.9];
fig4.Position = [10 10 500 350];
fig4.CurrentAxes.XLim = [0 2000];
ylabel('Mod. fitness')

legend({"Single-step $Q_2$", "Two-step $Q_2$", "Single-step $Q_1, Q_3$", "Two-step $Q_1, Q_3$","", "", "Max fitness", "Full functionality"}, 'Position',[0.7 0.2 0.1 0.2], 'Interpreter','latex');

print(fig4, '-dpdf', 'fitness_plot_2000_fit2.pdf');