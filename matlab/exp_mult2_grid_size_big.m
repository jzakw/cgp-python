experiments = {
    { "exp\mult2_mix", "Multiplicator2bit_4x4_levels_back_3_lambda_4", "$$4\times4\ n_l=3$$"}
    { "exp\mult2_levels_back", "Multiplicator2bit_5x5_levels_back_4", "$$5\times5\ n_l=4$$"}
    { "exp\mult2_mix", "Multiplicator2bit_6x6_levels_back_5_lambda_4", "$$6\times6\ n_l=5$$"}
    { "exp\mult2_mix", "Multiplicator2bit_7x7_levels_back_5_lambda_4", "$$7\times7\ n_l=6$$"}
    { "exp\mult2_big", "Multiplicator2bit_8x8_levels_back_7_lambda_4", "$$8\times8\ n_l=7$$"}
    { "exp\mult2_big", "Multiplicator2bit_9x9_levels_back_8_lambda_4", "$$9\times9\ n_l=8$$"}
    { "exp\mult2_big", "Multiplicator2bit_10x10_levels_back_9_lambda_4", "$$10\times10\ n_l=9$$"}
    { "exp\mult2_big", "Multiplicator2bit_11x11_levels_back_10_lambda_4", "$$11\times11\ n_l=10$$"}
    { "exp\mult2_big", "Multiplicator2bit_12x12_levels_back_11_lambda_4", "$$12\times12\ n_l=11$$"}
    { "exp\mult2_big", "Multiplicator2bit_13x13_levels_back_12_lambda_4", "$$13\times13\ n_l=12$$"}
    { "exp\mult2_big", "Multiplicator2bit_14x14_levels_back_13_lambda_4", "$$14\times14\ n_l=13$$"}
    { "exp\mult2_big", "Multiplicator2bit_15x15_levels_back_14_lambda_4", "$$15\times15\ n_l=14$$"}   
    };

experiments_data = load_data(experiments);
disp("Data loaded.");
%close all;
take = 0.5;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.functionality(k) == 1 && table.active_nodes(k) == 7 && table.longest_path(k) == 2;
[idx, max_gen, max_node_evals, fig1, fig2, data_fig1, data_fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "");

print(fig1, '-dpdf', 'grid_big_gens.pdf');
print(fig2, '-dpdf', 'grid_big_nodeevals.pdf');

%% CSV

[data_csv] = transform_to_csv(data_fig1);
csvwrite('grid_big_gens.csv', data_csv);
