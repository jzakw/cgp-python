experiments = {
    { "exp\mult2_mix", "Multiplicator2bit_4x4_levels_back_1_lambda_4", "$$4\times4\ n_l=1$$"}
    { "exp\mult2_grid_size", "Multiplicator2bit_4x4", "$$4\times4\ n_l=2$$"}
    { "exp\mult2_mix", "Multiplicator2bit_4x4_levels_back_3_lambda_4", "$$4\times4\ n_l=3$$"}
    
    { "exp\mult2_mix", "Multiplicator2bit_5x5_levels_back_1_lambda_4", "$$5\times5\ n_l=1$$"}
    { "exp\mult2_grid_size", "Multiplicator2bit_5x5", "$$5\times5\ n_l=2$$"}
    { "exp\mult2_levels_back", "Multiplicator2bit_5x5_levels_back_3", "$$5\times5\ n_l=3$$"}
    { "exp\mult2_levels_back", "Multiplicator2bit_5x5_levels_back_4", "$$5\times5\ n_l=4$$"}

    { "exp\mult2_mix", "Multiplicator2bit_6x6_levels_back_1_lambda_4", "$$6\times6\ n_l=1$$"}
    { "exp\mult2_grid_size", "Multiplicator2bit_6x6", "$$6\times6\ n_l=2$$"}
    { "exp\mult2_mix", "Multiplicator2bit_6x6_levels_back_3_lambda_4", "$$6\times6\ n_l=3$$"}
    { "exp\mult2_mix", "Multiplicator2bit_6x6_levels_back_4_lambda_4", "$$6\times6\ n_l=4$$"}
    { "exp\mult2_mix", "Multiplicator2bit_6x6_levels_back_5_lambda_4", "$$6\times6\ n_l=5$$"}

    { "exp\mult2_mix", "Multiplicator2bit_7x7_levels_back_1_lambda_4", "$$7\times7\ n_l=1$$"}
    { "exp\mult2_grid_size", "Multiplicator2bit_7x7", "$$7\times7\ n_l=2$$"}
    { "exp\mult2_mix", "Multiplicator2bit_7x7_levels_back_3_lambda_4", "$$7\times7\ n_l=3$$"}
    { "exp\mult2_mix", "Multiplicator2bit_7x7_levels_back_4_lambda_4", "$$7\times7\ n_l=4$$"}
    { "exp\mult2_mix", "Multiplicator2bit_7x7_levels_back_5_lambda_4", "$$7\times7\ n_l=5$$"}
    { "exp\mult2_mix", "Multiplicator2bit_7x7_levels_back_6_lambda_4", "$$7\times7\ n_l=6$$"}
    
    };

experiments_data = load_data(experiments);
disp("Data loaded.");
%close all;
take = 0.5;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.functionality(k) == 1 && table.active_nodes(k) == 7 && table.longest_path(k) == 2;
[idx, max_gen, max_node_evals, fig1, fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "");

print(fig1, '-dpdf', 'grid_gens.pdf');
print(fig2, '-dpdf', 'grid_nodeevals.pdf');