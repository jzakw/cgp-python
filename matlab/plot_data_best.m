function [idx, max_gen, max_node_evals, fig1, fig2, data_fig1, data_fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, experiments_label)
    lengths = zeros(1, length(experiments_data));
    for i = 1:length(experiments_data)
        lengths(i) = length(experiments_data{i});
    end
    if max(lengths) ~= min(lengths)
        disp("Different lengths: ["+num2str(lengths) + "]!");
    end
    exps_cnt = min(lengths);
    exps_top_cnt = ceil(exps_cnt*take);
    disp("Results for " + exps_cnt + " tests. Graphs contain only top "+ ceil(take*100) +" % runs - i.e. " + exps_top_cnt + " runs.")
    
    
    data = ones([length(experiments_data), exps_cnt]).*Inf;
    data2 = ones([length(experiments_data), exps_cnt]).*Inf;
    data_grouping = zeros(size(data));
    data_max_gen = ones(size(data)).*-1;

    labels = strings(1, length(experiments_data));
    for i = 1:length(experiments_data)
        labels(i) = experiments{i}{3};
    end
    
    for i = 1:length(experiments_data)
        for j = 1:exps_cnt
            table = experiments_data{i}{j};
            k = 1;
            data_grouping(i, j) = i;
            while k <= length(table.generation)
                if fun_eval(table, k, i)
                    data(i, j) = table.generation(k);
                    data2(i, j) = table.node_evals(k);
                    break
                end
                k = k+1;
            end
            if data(i, j) == Inf
                data_max_gen(i, j) = table.generation(end);
            end
        end
    end
    
    [data_sorted, idx] = sort(data, 2);

    data2_sorted = zeros(size(data_sorted));
    for i=1:size(data,1)
        data2_sorted(i, :) = data2(i, idx(i, :));
    end
    
    for i = 1:size(data_sorted, 1)
        for j = 1:size(data_sorted, 2)/2
            if data_sorted(i, j) > max(data_max_gen(i, :)) && max(data_max_gen(i, :)) > -1
                disp("Data for "  + labels(i) + " invalid! Generation " + data_sorted(i, j) + " used when execution ended at " + max(data_max_gen(i, :))+".");
            elseif data_sorted(i, j) == Inf
                disp("Data for "  + labels(i) + " invalid! Infinity value in first half.")
            end
        end
    end
    
    data_to_plot = data_sorted(:, 1:exps_top_cnt)';
    data2_to_plot = data2_sorted(:, 1:exps_top_cnt)';

    
    %hold on
    % Plot the box chart
    fig1 = figure('Renderer', 'painters', 'Position', [10 10 900 600]);
    tiledlayout(fig1, 1, 1, 'padding', 'tight');

    boxplot(data_to_plot, 'Labels', labels);
    set(gca,'TickLabelInterpreter','latex');
    xlabel(experiments_label);
    ylabel('Generations');
    title("Generací pro nejl. řešení (top "+ ceil(take*100) +" % z "+ exps_cnt +" běhů)")
    fig1.PaperSize = fig1.PaperPosition(3:4);
    
    
    fig2 = figure('Renderer', 'painters', 'Position', [10 10 900 600]);
    tiledlayout(fig2, 1, 1, 'padding', 'tight');
    boxplot(data2_to_plot, 'Labels', labels);
    set(gca,'TickLabelInterpreter','latex');
    xlabel(experiments_label);
    ylabel('Node evals');
    title("Ohodnocení hradel pro nejl. řešení (top "+ ceil(take*100) +" % z "+ exps_cnt +" běhů)")
    fig2.PaperSize = fig2.PaperPosition(3:4);

    max_gen = max(data_to_plot(data_to_plot<Inf), [], "all");
    max_node_evals = max(data2_to_plot(data2_to_plot<Inf), [], "all");

    data_fig1 = data_to_plot;
    data_fig2 = data2_to_plot;
end