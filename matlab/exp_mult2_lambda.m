experiments = {
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_1", "$$\lambda=1$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_2", "$$\lambda=2$$"}
    { "exp\mult2_grid_size", "Multiplicator2bit_5x5", "$$\lambda=4$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_8", "$$\lambda=8$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_16", "$$\lambda={16}$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_32", "$$\lambda={32}$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_64", "$$\lambda={64}$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_128", "$$\lambda={128}$$"}
    { "exp\mult2_lambda", "Multiplicator2bit_5x5_lambda_256", "$$\lambda={256}$$"}
    };

experiments_data = load_data(experiments);
%close all;
take = 0.5;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.functionality(k) == 1 && table.active_nodes(k) == 7 && table.longest_path(k) == 2;
[idx, max_gen, max_node_evals, fig1, fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "");


print(fig1, '-dpdf', 'lambda_gens.pdf');
print(fig2, '-dpdf', 'lambda_nodeevals.pdf');
