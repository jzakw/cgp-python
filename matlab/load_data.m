function [data] = load_data(experiments)
    
    experimentsDirectory = "..\";

    load 'opts.mat' opts;

    data = cell(1, length(experiments));
    for i = 1:length(experiments)
        data{i} = {};
    end
    
    
    for j = 1:length(experiments)
        subfolders = dir(experimentsDirectory+"\"+experiments{j}{1});
        for i = length(subfolders):-1:1%1:length(subfolders)%
            if startsWith(subfolders(i).name, experiments{j}{2}+"_")
                filePath = fullfile(subfolders(i).folder, subfolders(i).name) + "\results.csv";
                exp_data = readtable(filePath, opts);
                table_size = size(exp_data);
                if(table_size(2) ~= 12)
                    disp("Invalid input " + subfolders(i).name)
                end
    
                data{j}{end+1} = exp_data;
            end
        end
    end
end