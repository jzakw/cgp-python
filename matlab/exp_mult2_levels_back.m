experiments = {
    { "mult2_levels_back", "Multiplicator2bit_5x5_levels_back_1", "LB1"}
    { "mult2_grid_size", "Multiplicator2bit_5x5", "LB2"}
    { "mult2_levels_back", "Multiplicator2bit_5x5_levels_back_3", "LB3"}
    { "mult2_levels_back", "Multiplicator2bit_5x5_levels_back_4", "LB4"}
    };

experiments_data = load_data(experiments);
close all;
plot_data_box(experiments, experiments_data, "Levels back");