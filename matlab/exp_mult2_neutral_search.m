experiments = {
    { "exp\mult2_grid_size", "Multiplicator2bit_5x5", "Neutral search"}
    { "exp\mult2_neutral_search", "Multiplicator2bit_biased", "Biased search"}
    };

experiments_data = load_data(experiments);
disp("Data loaded.");
%close all;
take = 1;

%% Znázornění nejlepších průběhů
fun_eval = @(table, k, i)table.generation(k) == 100000;
[idx, max_gen, max_node_evals, fig1, fig2] = plot_data_best(experiments, experiments_data, take, fun_eval, "Initial random set size");
close(fig1)
close(fig2)

%% Gen 300
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fig3 = plot_data_gen(experiments, experiments_data, take, idx, 300, fun_cmp, fun_val, "", 343);

fig3.CurrentAxes.YLim = [281 349];
fig3.Position = [10 10 400 350];
print(fig3, '-dpdf', 'neutral_search_gen1000.pdf');

%% Gen 3000
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fig3 = plot_data_gen(experiments, experiments_data, take, idx, 3000, fun_cmp, fun_val, "", 343);

fig3.CurrentAxes.YLim = [281 349];
fig3.Position = [10 10 400 350];
print(fig3, '-dpdf', 'neutral_search_gen10000.pdf');

%% Gen 30000
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fig3 = plot_data_gen(experiments, experiments_data, take, idx, 30000, fun_cmp, fun_val, "", 343);

fig3.CurrentAxes.YLim = [281 349];
fig3.Position = [10 10 400 350];
print(fig3, '-dpdf', 'neutral_search_gen100000.pdf');

%%
fun_cmp = @(table, k, i)table.generation(k);
fun_val = @(table, k, i)table.fitness(k);
fun_process = @(x, dim)[quantile(x,0.5, dim), quantile(x,0.25, dim), quantile(x,0.75,dim)];
fig3 = plot_data_progress(experiments, experiments_data, take, idx, max_gen, fun_cmp, fun_val, fun_process, "Generations", 10000);
fig3.CurrentAxes.YLim = [281 349];
fig3.CurrentAxes.XLim = [0 10000];

legend({"Neutral search $Q_2$", "Biased search $Q_2$", "Neutral search $Q_1, Q_3$", "Biased search $Q_1, Q_3$","", "", "Max fitness"}, 'Position',[0.7 0.2 0.1 0.2], 'Interpreter','latex');

print(fig3, '-dpdf', 'neutral_search_progress.pdf');