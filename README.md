# Evolutionary Algorithm based on Cartesian Genetic Programming (CGP)

This project implements an evolutionary algorithm based on Cartesian Genetic Programming (CGP) in Python. CGP is an evolutionary algorithm used not only for evolving combinatory circuits.

## Features

- Implements the CGP evolutionary algorithm for evolving combinatory circuits.
- Provides tools for visualizing individuals and evolution results.
- Supports loading problems and saving evolution results.

## Installation

1. Clone this repository to your computer.
2. Install graphviz, for more information visit [graphviz.org](https://graphviz.org/download/). On Debian simply call `sudo apt install graphviz`.
3. Consider creating virtual environment, i.e. `python -m venv venv && source venv/bin/activate`.
4. Install dependencies using the command: `pip install -r requirements.txt`.

## CLI Usage

Run the `src/cgp_cli.py` script with appropriate arguments. For more information, run the script with the `-h` flag:

```
python src/cgp_cli.py -h
```

Note that the backup of the problem will be always saved to `bac` folder in current location.

## Example Usage

1. Load and visualize a backup of the execution:

```
python src/cgp_cli.py -f out/run_1 -v
```

2. Load a demo and perform evolution:

```
python src/cgp_cli.py -d TestProblem
```

# Documentation

Build documentation by running `pydoctor`. Documentation will be placed in `public/docs` folder. Or view the documentation on [GitLab Pages](https://cgp-python-jzakw-3ca602a5e56a93650bb6768a540046132806e98a9d9fec.gitlab.io/docs/).

# Authors

- Jan Žák

## License

This project is licensed under the MIT License. For more information, please see the LICENSE file.
