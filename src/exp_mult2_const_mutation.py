""" Experiment with different grid sizes for 2-bit multiplicator. """

from cgp.cgp_problem import multiplicator_fn, CGPProblem, CGPProblemStopConditions, CGPProblemFitnessParams, CGPStrategyOnePlusLambda, CGPMutationConstantMutationRate, CGPBasicGates
from cgp_experiment import Experiment

BACKUP_FOLDER = "exp/mult2_const_mutation"
TEST_COUNT = 50


def run() -> None:
    """ Run the experiment """
    problems = [
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_mr_0.01",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationConstantMutationRate(
                mutation_rate=0.01),  # One mutation
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=2000000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_mr_0.02",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationConstantMutationRate(
                mutation_rate=0.02),  # Two mutations
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=2000000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_mr_0.03",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationConstantMutationRate(
                mutation_rate=0.03),  # Three mutations
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=2000000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_mr_0.1",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationConstantMutationRate(
                mutation_rate=0.1),  # 8 mutations
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=2000000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_mr_0.2",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationConstantMutationRate(
                mutation_rate=0.2),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=2000000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_mr_0.3",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationConstantMutationRate(
                mutation_rate=0.3),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=2000000),
        ),
    ]

    experiment = Experiment(problems, BACKUP_FOLDER, TEST_COUNT)
    experiment.run()


if __name__ == "__main__":
    run()
