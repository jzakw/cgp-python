""" Experiment with different grid sizes for 2-bit multiplicator. """

from cgp.cgp_problem import multiplicator_fn, CGPProblem, CGPProblemStopConditions, CGPProblemFitnessParams, CGPStrategyOnePlusLambda, CGPMutationGoldman, CGPBasicGates
from cgp_experiment import Experiment

BACKUP_FOLDER = "exp/mult2_lambda"
TEST_COUNT = 50


def run() -> None:
    """ Run the experiment """
    problems = [
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_1",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=1),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=600000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_2",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=2),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=600000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_8",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=8),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=150000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_16",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=16),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=75000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_32",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=32),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=37500),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_64",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=64),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=20000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_128",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=128),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=30000),
        ),
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_lambda_256",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=256),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=20000),
        )
    ]

    experiment = Experiment(problems, BACKUP_FOLDER, TEST_COUNT)
    experiment.run()


if __name__ == "__main__":
    run()
