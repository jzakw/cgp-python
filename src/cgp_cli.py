""" Command Line Interface for CGP """

import sys
from cgp.cgp_commands import CGPCommands
from cgp.cgp_executor import CGPExecutor
from cgp.cgp_problem import CGPBasicProblems


class CGPCli(object):
    """Command Line Interface for CGP"""

    @staticmethod
    def run() -> None:
        """Simple CLI interface."""
        CGPCommands.init("-a" in sys.argv)
        if "-a" in sys.argv:
            print(f"[-a] Commands disabled during execution.")
        while True:
            CGPCli.execute()
            if "-l" not in sys.argv or CGPCommands.exit_flag:
                break
        CGPCommands.destroy()

    @staticmethod
    def execute() -> None:
        if "-h" in sys.argv:
            CGPCli.display_help()
            return

        backup_path = None
        if "-w" in sys.argv:
            backup_index = sys.argv.index("-w") + 1
            if backup_index < len(sys.argv):
                backup_path = sys.argv[backup_index]
                print(f"[-w] Saving backup to {backup_path}")

        if "-f" in sys.argv:
            # Use provdided file
            file_index = sys.argv.index("-f") + 1
            if file_index < len(sys.argv):
                file_name = sys.argv[file_index]
                print(f"[-f] Loading backup from {file_name}")

                generation = -1
                if "-n" in sys.argv:
                    print(
                        "[-n] Loading only the problem. Initializating population from scratch."
                    )
                    generation = None
                elif "-g" in sys.argv:
                    generation_index = sys.argv.index("-g") + 1
                    generation = int(sys.argv[generation_index])
                    print(f"[-g] Loading generation {generation}")

                cgpExecutor = CGPExecutor.restore(
                    file_name, generation, backup_path, log_enabled=True, visualize_enabled=True)
                CGPCommands.register_executor(cgpExecutor)
                if "-v" in sys.argv:
                    print("[-v] Visualizing the individual.")
                    cgpExecutor.visualize(
                        sys.argv[(sys.argv.index("-v") + 1):])
                else:
                    print("[-f] Executing...")
                    cgpExecutor.execute()
            else:
                print("[-f] Error: File name not provided after -f flag.")
            return
        elif "-d" in sys.argv:
            # Use demo from CGPBasicProblems
            demo_index = sys.argv.index("-d") + 1
            if demo_index < len(sys.argv):
                problem_label = sys.argv[demo_index]
                print(f"[-d] Loading demo problem {problem_label}")
                problem = [
                    problem
                    for problem in CGPBasicProblems.DemoProblems
                    if problem.label == problem_label
                ]
                cgpExecutor = CGPExecutor(
                    problem[0], backup_path, log_enabled=True, visualize_enabled=True)
                CGPCommands.register_executor(cgpExecutor)
                print("[-d] Executing...")
                cgpExecutor.execute()
            else:
                print("[-d] Error: Demo name not provided after -d flag.")
            return

    @staticmethod
    def display_help() -> None:
        """Display help message."""
        print(
            """
    Usage: python cgp.py [options]

    Options:
    -f <filename>     Load backup from the specified file.
    -d <demo>         Load a demo problem from CGPBasicProblems.
    -n                Load only the problem and initialize population from scratch.
    -g <generation>   Load specific generation from the backup file.
    -v <parameters>   Only visualize the individual with specified parameters.
    -w <path>         Save backup to the specified folder.
    -l                Repeat the experiment when conditions match until manually stopped. 
    -a                Run in headless mode. Disable commands during execution.
    -h                Display this help message.

    Examples:
    python cgp.py -f test              # Load backup from a folder 'test'.
    python cgp.py -d TestProblem       # Load a demo problem named 'TestProblem'.
    python cgp.py -f test1 -n -w test2 # Load problem from backup and initialize population from scratch. Save outputs to 'test2'.
    python cgp.py -f test -g 10        # Load generation 10 from the backup.
    python cgp.py -f test -v           # Visualize the individual.
    python cgp.py -f test -v param1 param2   # Visualize the individual with specific parameters for visualization.
    """
        )


if __name__ == "__main__":
    CGPCli.run()
