""" Experiment with different grid sizes for 2-bit multiplicator. """

from cgp.cgp_problem import multiplicator_fn, CGPProblem, CGPProblemStopConditions, CGPProblemFitnessParams, CGPStrategyOnePlusLambda, CGPMutationGoldman, CGPBasicGates
from cgp_experiment import Experiment
import numpy as np
import numpy.typing as npt

BACKUP_FOLDER = "exp/exp_hammings_encoder"
TEST_COUNT = 30


def hamming_7_4_encoder(binary_array: npt.NDArray[np.int8]) -> npt.NDArray[np.int8]:
    hamming_code = np.zeros(7, dtype=np.int8)

    # Data bits
    hamming_code[2] = binary_array[0]
    hamming_code[4] = binary_array[1]
    hamming_code[5] = binary_array[2]
    hamming_code[6] = binary_array[3]

    # Parity bits
    hamming_code[0] = (hamming_code[2] + hamming_code[4] + hamming_code[6]) % 2
    hamming_code[1] = (hamming_code[2] + hamming_code[5] + hamming_code[6]) % 2
    hamming_code[3] = (hamming_code[4] + hamming_code[5] + hamming_code[6]) % 2

    return hamming_code[[0, 1, 3]]


def hamming_15_11_encoder(binary_array: npt.NDArray[np.int8]) -> npt.NDArray[np.int8]:
    hamming_code = np.zeros(15, dtype=np.int8)

    # Data
    hamming_code[2] = binary_array[0]
    hamming_code[4] = binary_array[1]
    hamming_code[5] = binary_array[2]
    hamming_code[6] = binary_array[3]
    hamming_code[8] = binary_array[4]
    hamming_code[9] = binary_array[5]
    hamming_code[10] = binary_array[6]
    hamming_code[11] = binary_array[7]
    hamming_code[12] = binary_array[8]
    hamming_code[13] = binary_array[9]
    hamming_code[14] = binary_array[10]

    # Parity
    hamming_code[0] = (hamming_code[2] + hamming_code[4] + hamming_code[6] +
                       hamming_code[8] + hamming_code[10] + hamming_code[12] + hamming_code[14]) % 2
    hamming_code[1] = (hamming_code[2] + hamming_code[5] + hamming_code[6] +
                       hamming_code[9] + hamming_code[10] + hamming_code[13] + hamming_code[14]) % 2
    hamming_code[3] = (hamming_code[4] + hamming_code[5] + hamming_code[6] +
                       hamming_code[11] + hamming_code[12] + hamming_code[13] + hamming_code[14]) % 2
    hamming_code[7] = (hamming_code[8] + hamming_code[9] + hamming_code[10] +
                       hamming_code[11] + hamming_code[12] + hamming_code[13] + hamming_code[14]) % 2

    return hamming_code[[0, 1, 3, 7]]


def run() -> None:
    """ Run the experiment """
    problems = [
        CGPProblem.auto_tests(
            label="Exp4_Hamming_7_4_encoder_xor",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=3,
            gates_arity=2,
            gates=[CGPBasicGates.xor_gate],
            problem_fn=lambda input: hamming_7_4_encoder(input),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(generations=100000),
        ),
        CGPProblem.auto_tests(
            label="Exp4_Hamming_7_4_encoder_nand",
            columns_cnt=8,
            rows_cnt=12,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=3,
            gates_arity=2,
            gates=[CGPBasicGates.nand_gate],
            problem_fn=lambda input: hamming_7_4_encoder(input),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=10,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(generations=1000000),
        )
    ]

    experiment = Experiment(problems, BACKUP_FOLDER, TEST_COUNT)
    experiment.run()


if __name__ == "__main__":
    run()
