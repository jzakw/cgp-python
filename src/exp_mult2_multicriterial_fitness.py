""" Experiment with different grid sizes for 2-bit multiplicator. """

from cgp.cgp_problem import multiplicator_fn, CGPProblem, CGPProblemStopConditions, CGPProblemFitnessParams, CGPStrategyOnePlusLambda, CGPMutationGoldman, CGPBasicGates
from cgp_experiment import Experiment

BACKUP_FOLDER = "exp/mult2_multicriterial_fitness"
TEST_COUNT = 50


def run() -> None:
    """ Run the experiment """
    problems = [
        CGPProblem.auto_tests(
            label="Multiplicator2bit_5x5_twostep",
            columns_cnt=5,
            rows_cnt=5,
            levels_back=2,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                area_coef=0.2,
                time_coef=0.2,
                fn_coef=None,
                fn_reward=None,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=300000),
        )
    ]

    experiment = Experiment(problems, BACKUP_FOLDER, TEST_COUNT)
    experiment.run()


if __name__ == "__main__":
    run()
