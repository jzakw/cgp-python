""" CGPProblem class and basic problems for CGP """

from dataclasses import asdict, dataclass
from types import NoneType
from typing import Callable
import numpy as np
import numpy.typing as npt
from itertools import product
from .cgp_gate import CGPGate, CGPBasicGates
import yaml


@dataclass
class CGPStrategy:
    """Class representing CGP strategy"""
    code: str
    """Strategy code"""


@dataclass
class CGPStrategyOnePlusLambda(CGPStrategy):
    """(1+lambda) Evolution Strategy"""

    code: str = "ONE_PLUS_LAMBDA"
    """Strategy code"""
    lambda_coef: int = 4
    """Lambda coefficient"""


@dataclass
class CGPMutation:
    """Enum representing CGP mutation"""

    code: str
    """Mutation code"""


@dataclass
class CGPMutationConstantMutationRate(CGPMutation):
    """Constant Mutation Rate Mutation Strategy"""

    code: str = "CONST_MUTATION_RATE"
    """Mutation code"""
    mutation_rate: np.float32 = 0.01
    """Mutation rate"""


@dataclass
class CGPMutationSimulatedAnnealing(CGPMutation):
    """Simulated Annealing Mutation Strategy"""

    code: str = "SIMULATED_ANNEALING"
    """Mutation code"""
    cooling_rate: np.float32 = 0.95
    """Cooling rate"""
    start_mutation_rate: np.float32 = 0.5
    """Start mutation rate"""


@dataclass
class CGPMutationGoldman(CGPMutation):
    """Goldman Mutation Strategy"""

    code: str = "GOLDMAN_MUTATION"
    """Mutation code"""


@dataclass
class CGPProblemFitnessParams(object):
    """Fitness parameters"""

    fn_coef: np.float32 | NoneType
    """Functionality coefficient"""
    area_coef: np.float32
    """Area coefficient"""
    time_coef: np.float32
    """Time coefficient"""
    fn_reward: np.float32 | NoneType
    """Functionality reward"""


@dataclass
class CGPProblemStopConditions(object):
    """Stop conditions"""

    functionality: np.float32 = 1.0
    """Functionality value between 0 and 1 - 1 is best"""
    active_nodes: np.int16 | NoneType = None
    """Number of active nodes"""
    longest_path: np.int16 | NoneType = None
    """Longest path in the graph"""
    generations: int | NoneType = None
    """Number of generations"""


class CGPProblem:
    """Class representing a CGP problem"""

    label: str
    """Label of the problem"""

    columns_cnt: np.int16
    """Number of columns"""
    rows_cnt: np.int16
    """Number of rows"""
    levels_back: np.int16
    """Number of levels back"""
    inputs_cnt: np.int16
    """ Inputs count"""
    outputs_cnt: np.int16
    """Outputs count"""

    gates_arity: np.int16
    """Arity of the gates"""
    gates: list[CGPGate]
    """List of used functions"""
    gates_cnt: np.int16 = 0
    """Number of gates"""
    gates_map: npt.NDArray[np.int8]
    """Map of gates and its outputs"""

    strategy: CGPStrategy
    """Strategy used for CGP"""

    mutation: CGPMutation
    """Strategy used for CGP"""

    fitness_params: CGPProblemFitnessParams
    """Fitness parameters"""

    test_inputs: npt.NDArray[np.int8] = None
    """Set of problem test inputs"""
    test_outputs: npt.NDArray[np.int8] = None
    """Set of expected problem test outputs"""
    tests_cnt: np.int16 = 0
    """Number of tests"""

    levels_back_nodes: np.int16 = 0
    """ Number of rows * levels back """
    nodes_cnt: np.int16 = 0
    """ Number nodes = rows * columns """
    nodes_genes_cnt: np.int16 = 0
    """ Number of nodes * (arity + 1) """
    genes_cnt: np.int16 = 0
    """ Number of genes in genotype """

    stop_conditions: CGPProblemStopConditions
    """ Parameters for the check_conditions function """

    def __init__(
        self,
        label: str,
        columns_cnt: np.int16,
        rows_cnt: np.int16,
        levels_back: np.int16,
        inputs_cnt: np.int16,
        outputs_cnt: np.int16,
        gates_arity: np.int16,
        gates: list[CGPGate],
        strategy: CGPStrategy,
        mutation: CGPMutation,
        fitness_params: CGPProblemFitnessParams,
        test_inputs: npt.NDArray[np.int8] = None,
        test_outputs: npt.NDArray[np.int8] = None,
        stop_conditions: CGPProblemStopConditions = CGPProblemStopConditions(),
    ):
        """Constructor of the CGPProblem class"""

        self.label = label
        self.columns_cnt = columns_cnt
        self.rows_cnt = rows_cnt
        self.levels_back = levels_back
        self.inputs_cnt = inputs_cnt
        self.outputs_cnt = outputs_cnt
        self.gates_arity = gates_arity
        self.gates = gates
        self.strategy = strategy
        self.mutation = mutation
        self.fitness_params = fitness_params

        self.gates_cnt = len(gates)
        self.test_inputs = test_inputs
        self.test_outputs = test_outputs
        self.tests_cnt = test_inputs.shape[0] if test_inputs is not None else 0

        self.levels_back_nodes = self.rows_cnt * self.levels_back
        self.nodes_cnt = self.rows_cnt * self.columns_cnt
        self.nodes_genes_cnt = self.nodes_cnt * (self.gates_arity + 1)
        self.genes_cnt = self.nodes_genes_cnt + self.outputs_cnt

        self.stop_conditions = stop_conditions

        for gate in self.gates:
            gate.calculate_fn_map(self.gates_arity)

        self.gates_map = np.array(
            [gate.fn_map for gate in self.gates], dtype=np.int8)

    @classmethod
    def auto_tests(
        cls,
        label: str,
        columns_cnt: np.int16,
        rows_cnt: np.int16,
        levels_back: np.int16,
        inputs_cnt: np.int16,
        outputs_cnt: np.int16,
        gates_arity: np.int16,
        gates: list[CGPGate],
        problem_fn: Callable[[npt.NDArray[np.int8]], npt.NDArray[np.int8]],
        strategy: CGPStrategy,
        mutation: CGPMutation,
        fitness_params: CGPProblemFitnessParams,
        stop_conditions: CGPProblemStopConditions = CGPProblemStopConditions(),
    ) -> "CGPProblem":
        """Create a new CGPProblem with automatically generated test inputs and outputs"""
        problem = cls(
            label=label,
            columns_cnt=columns_cnt,
            rows_cnt=rows_cnt,
            levels_back=levels_back,
            inputs_cnt=inputs_cnt,
            outputs_cnt=outputs_cnt,
            gates_arity=gates_arity,
            gates=gates,
            strategy=strategy,
            mutation=mutation,
            fitness_params=fitness_params,
            stop_conditions=stop_conditions,
        )
        problem.generate_inputs()
        problem.generate_outputs(problem_fn)
        return problem

    def generate_inputs(self) -> npt.NDArray[np.int8]:
        """Generate all possible inputs for given inputs count

        @returns: Set of all possible inputs
        """
        self.test_inputs = np.array(
            list(product([0, 1], repeat=self.inputs_cnt)), dtype=np.int8
        )
        self.tests_cnt = self.test_inputs.shape[0]
        return self.test_inputs

    def generate_outputs(
        self, problem_fn: Callable[[npt.NDArray[np.int8]], npt.NDArray[np.int8]]
    ) -> npt.NDArray[np.int8]:
        """Generate expected outputs for all inputs

        @arg problem_fn: Function that calculates output for given input

        @returns: Set of expected outputs
        """
        self.test_outputs = np.empty(
            [self.test_inputs.shape[0], self.outputs_cnt], dtype=np.int8
        )
        for i in range(self.test_inputs.shape[0]):
            self.test_outputs[i, :] = problem_fn(self.test_inputs[i, :])
        return self.test_outputs

    def backup(self, filename: str):
        """Backups the problem to a file

        @arg filename: File to write to
        """
        with open(filename, "w") as file:
            data = {
                "label": self.label,
                "columns_cnt": self.columns_cnt,
                "rows_cnt": self.rows_cnt,
                "levels_back": self.levels_back,
                "inputs_cnt": self.inputs_cnt,
                "outputs_cnt": self.outputs_cnt,
                "gates_arity": self.gates_arity,
                "gates": [gate.label for gate in self.gates],
                "strategy": asdict(self.strategy),
                "mutation": asdict(self.mutation),
                "fitness_params": asdict(self.fitness_params),
                "test_inputs": self.test_inputs.tolist(),
                "test_outputs": self.test_outputs.tolist(),
                "stop_conditions": asdict(self.stop_conditions),
            }
            yaml.dump(data, file)

    @classmethod
    def restore(cls, filename: str) -> "CGPProblem":
        """Restores the problem from a file

        @arg filename: File to read from

        @returns: Restored problem
        """
        with open(filename, "r") as file:
            data = yaml.safe_load(file)

        gates = []
        for gate_label in data["gates"]:
            gate = [obj for obj in CGPBasicGates.GroupAll if obj.label == gate_label]
            if len(gate) == 1:
                gates.append(gate[0])
            else:
                raise ValueError(f"Gate {gate_label} not found")

        return cls(
            label=data["label"],
            columns_cnt=data["columns_cnt"],
            rows_cnt=data["rows_cnt"],
            levels_back=data["levels_back"],
            inputs_cnt=data["inputs_cnt"],
            outputs_cnt=data["outputs_cnt"],
            gates_arity=data["gates_arity"],
            gates=gates,
            strategy=(
                CGPStrategyOnePlusLambda(**data["strategy"])
                if data["strategy"]["code"] == "ONE_PLUS_LAMBDA"
                else NotImplementedError("Strategy not implemented")
            ),
            mutation=(
                CGPMutationConstantMutationRate(**data["mutation"])
                if data["mutation"]["code"] == "CONST_MUTATION_RATE"
                else (
                    CGPMutationSimulatedAnnealing(**data["mutation"])
                    if data["mutation"]["code"] == "SIMULATED_ANNEALING"
                    else (
                        CGPMutationGoldman(**data["mutation"])
                        if data["mutation"]["code"] == "GOLDMAN_MUTATION"
                        else NotImplementedError("Mutation not implemented")
                    )
                )
            ),
            fitness_params=CGPProblemFitnessParams(**data["fitness_params"]),
            test_inputs=np.array(data["test_inputs"], dtype=np.int8),
            test_outputs=np.array(data["test_outputs"], dtype=np.int8),
            stop_conditions=CGPProblemStopConditions(
                **data["stop_conditions"]),
        )


def decimal_to_binary_array(decimal_num: int, length: np.int16) -> npt.NDArray[np.int8]:
    """Converts a decimal number to a binary array of a given length

    @arg decimal_num: Decimal number to be converted
    @arg length: Binary array length

    @returns: Binary array
    """
    binary_str = np.binary_repr(
        decimal_num, width=length
    )  # Convert number to binary string
    binary_array = np.array(
        [np.int8(bit) for bit in binary_str], dtype=np.int8
    )  # Convert binary string to np.array

    return binary_array


def binary_array_to_decimal(binary_array: npt.NDArray[np.int8]) -> int:
    """Converts a binary array to a decimal number

    @arg binary_array: Binary array to be converted

    @returns: Decimal number
    """
    decimal_num = np.sum(
        binary_array[::-1] * (2 ** np.arange(len(binary_array)))
    )  # Convert binary array to decimal number - reverse array and multiply by 2^i - BigEndian

    return int(decimal_num)


def multiplicator_fn(
    input: npt.NDArray[np.int8], bit_in1_cnt: int, bit_in2_cnt: int
) -> npt.NDArray[np.int8]:
    """
    Multiplicator function for CGPProblem

    @arg input: binary array of inputs
    @arg bit_in1_cnt: number of bits for input 1
    @arg bit_in2_cnt: number of bits for input 2

    @returns: Product of two numbers - binary array - length of bit_in1_cnt + bit_in2_cnt
    """
    x = binary_array_to_decimal(input[0:bit_in1_cnt])
    y = binary_array_to_decimal(input[bit_in1_cnt: bit_in1_cnt + bit_in2_cnt])
    return decimal_to_binary_array(x * y, (bit_in1_cnt + bit_in2_cnt))


def adder_fn(
    input: npt.NDArray[np.int8], bit_in1_cnt: int, bit_in2_cnt: int
) -> npt.NDArray[np.int8]:
    """
    Adder function for CGPProblem

    @arg input: binary array of inputs
    @arg bit_in1_cnt: number of bits for input 1
    @arg bit_in2_cnt: number of bits for input 2

    @returns: Sum of two numbers - binary array - length of max(bit_in1_cnt, bit_in2_cnt) + 1
    """
    x = binary_array_to_decimal(input[0:bit_in1_cnt])
    y = binary_array_to_decimal(input[bit_in1_cnt: bit_in1_cnt + bit_in2_cnt])
    return decimal_to_binary_array(x + y, max(bit_in1_cnt, bit_in2_cnt) + 1)


def parity_fn(input: npt.NDArray[np.int8]) -> npt.NDArray[np.int8]:
    """
    Parity function for CGPProblem

    @arg input: binary array of inputs

    @returns: Parity of the input array
    """
    return np.array([np.sum(input) % 2], dtype=np.int8)


class CGPBasicProblems(object):
    """Basic problems for CGP"""

    TestProblem = CGPProblem.auto_tests(
        label="TestProblem",
        columns_cnt=5,
        rows_cnt=5,
        levels_back=2,
        inputs_cnt=2,
        outputs_cnt=3,
        gates_arity=2,
        gates=[CGPBasicGates.or_gate,
               CGPBasicGates.and_gate, CGPBasicGates.not_gate],
        problem_fn=lambda input: np.array(
            [1 if (input[0] * input[1] > 0) else 0, input[0], input[1]], dtype=np.int8
        ),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        stop_conditions=CGPProblemStopConditions(active_nodes=1),
    )
    """Simple Test Problem"""

    Multiplicator2bit = CGPProblem.auto_tests(
        label="Multiplicator2bit",
        columns_cnt=5,
        rows_cnt=5,
        levels_back=2,
        inputs_cnt=4,
        outputs_cnt=4,
        gates_arity=2,
        gates=CGPBasicGates.Group2,
        problem_fn=lambda input: multiplicator_fn(input, 2, 2),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        stop_conditions=CGPProblemStopConditions(
            active_nodes=7, longest_path=2),
    )
    """2 bit Multiplicator Problem"""

    Adder2bit = CGPProblem.auto_tests(
        label="Adder2bit",
        columns_cnt=5,
        rows_cnt=5,
        levels_back=2,
        inputs_cnt=4,
        outputs_cnt=3,
        gates_arity=2,
        gates=CGPBasicGates.Group2,
        problem_fn=lambda input: adder_fn(input, 2, 2),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        stop_conditions=CGPProblemStopConditions(
            active_nodes=7, longest_path=3),
    )
    """2 bit Adder Problem"""

    Multiplicator3bit = CGPProblem.auto_tests(
        label="Multiplicator3bit",
        columns_cnt=8,
        rows_cnt=8,
        levels_back=2,
        inputs_cnt=6,
        outputs_cnt=6,
        gates_arity=2,
        gates=CGPBasicGates.Group2,
        problem_fn=lambda input: multiplicator_fn(input, 3, 3),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=10,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        # check_conditions= 29 active nodes, 6 longest path
    )
    """3 bit Multiplicator Problem"""

    Adder3bit = CGPProblem.auto_tests(
        label="Adder3bit",
        columns_cnt=6,
        rows_cnt=6,
        levels_back=1,
        inputs_cnt=6,
        outputs_cnt=4,
        gates_arity=2,
        gates=CGPBasicGates.Group2,
        problem_fn=lambda input: adder_fn(input, 3, 3),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        # check_conditions= 12 active nodes, 5 longest path
    )
    """3 bit Adder Problem"""

    Multiplicator4bit = CGPProblem.auto_tests(
        label="Multiplicator4bit",
        columns_cnt=8,
        rows_cnt=8,
        levels_back=2,
        inputs_cnt=8,
        outputs_cnt=8,
        gates_arity=2,
        gates=CGPBasicGates.Group2,
        problem_fn=lambda input: multiplicator_fn(input, 4, 4),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
    )
    """4 bit Multiplicator Problem"""

    Adder4bit = CGPProblem.auto_tests(
        label="Adder3bit",
        columns_cnt=9,
        rows_cnt=9,
        levels_back=2,
        inputs_cnt=8,
        outputs_cnt=5,
        gates_arity=2,
        gates=CGPBasicGates.Group2,
        problem_fn=lambda input: adder_fn(input, 4, 4),
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        # check_conditions= 17 active nodes, 7 longest path
    )
    """4 bit Adder Problem"""

    Xor2bitNor = CGPProblem.auto_tests(
        label="Xor2bitNor",
        columns_cnt=3,
        rows_cnt=3,
        levels_back=1,
        inputs_cnt=2,
        outputs_cnt=1,
        gates_arity=2,
        gates=[CGPBasicGates.nor_gate],
        problem_fn=lambda input: input[0] ^ input[1],
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        stop_conditions=CGPProblemStopConditions(
            active_nodes=5, longest_path=3),
    )
    """2 bit XOR Problem with NOR gate"""

    Xor2bitNand = CGPProblem.auto_tests(
        label="Xor2bitNand",
        columns_cnt=3,
        rows_cnt=3,
        levels_back=1,
        inputs_cnt=2,
        outputs_cnt=1,
        gates_arity=2,
        gates=[CGPBasicGates.nand_gate],
        problem_fn=lambda input: input[0] ^ input[1],
        strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
        mutation=CGPMutationGoldman(),
        fitness_params=CGPProblemFitnessParams(
            fn_coef=5,
            area_coef=1,
            time_coef=1,
            fn_reward=2,
        ),
        stop_conditions=CGPProblemStopConditions(
            active_nodes=4, longest_path=3),
    )
    """2 bit XOR Problem with NOR gate"""

    DemoProblems = [
        TestProblem,
        Multiplicator2bit,
        Multiplicator3bit,
        Multiplicator4bit,
        Adder2bit,
        Adder3bit,
        Adder4bit,
        Xor2bitNor,
        Xor2bitNand,
    ]
    """List of demo problems"""
