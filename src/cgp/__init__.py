""" Package containing the implementation of Cartesian Genetic Programming (CGP) algorithm."""

VERSION = "3.0"
""" Module version """
