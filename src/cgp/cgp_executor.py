""" Manager for the CGP algorithm execution. """

import csv
from dataclasses import asdict, dataclass
import datetime
from enum import Enum
import os
import time
import sys
import numpy as np
import numpy.typing as npt

from .cgp_utils import generate_short_id
from .cgp_individual import CGPIndividual
from .cgp_population import CGPPopulation
from .cgp_problem import (
    CGPMutationConstantMutationRate,
    CGPMutationGoldman,
    CGPMutationSimulatedAnnealing,
    CGPProblem,
    CGPStrategyOnePlusLambda,
)
from .cgp_visualizer import CGPVisualizer

CSV_FIELDNAMES = [
    "generation",
    "fitness",
    "functionality",
    "active_nodes",
    "longest_path",
    "time",
    "gen_time",
    "nc_gens",
    "fitness_evals",
    "graph_evals",
    "node_evals",
    "genotype",
]
""" Field names in log CSV file """

CSV_PATH = "results.csv"
"""Path to log CSV file in the folder"""
CONFIG_PATH = "problem.yml"
"""Path to problem configuration file in the folder"""

BACKUP_GENERATION = 100
"""Interval for backup in generations"""


class CGPExecutorState(Enum):
    """Enum for executor state"""

    IDLE = 0
    """Idle state"""
    RUNNING = 1
    """Running state"""
    CAPTURING = 2
    """Capturing state"""
    STOPPING = 4
    """Stopping state"""


@dataclass
class CGPExecutorBackupData(object):
    """Data for the backup of the executor"""
    generation: np.int32
    """ Generation number """
    fitness: np.float32
    """ Best fitness """
    functionality: np.float32
    """ Functionality of the best individual """
    active_nodes: np.int16
    """ Number of active nodes in the best individual """
    longest_path: np.int16
    """ Longest path in the best individual """
    time: np.float32
    """ Time from the start """
    gen_time: np.float32
    """ Time per generation """
    nc_gens: np.int32
    """ Number of generations without change from start """
    fitness_evals: np.int64
    """ Number of fitness evaluations from start """
    graph_evals: np.float64
    """ Number of graph evaluations from start """
    node_evals: np.float64
    """ Number of node evaluations from start """
    genotype: npt.NDArray[np.int16]
    """ Genotype of the best individual """


class CGPExecutor:
    """Manager for the CGP algorithm execution."""

    problem: CGPProblem
    """Problem to solve"""
    population: CGPPopulation
    """ Population of individuals """
    start_time: float
    """ Time of start """

    backup_folder: str = None
    """ Folder for backup """
    state: CGPExecutorState = CGPExecutorState.IDLE
    """ State of the executor """
    capturing_args: list[str] = []
    """ Arguments for the capturing """
    max_fitness: float
    """ Maximum fitness """

    backup_data: CGPExecutorBackupData = CGPExecutorBackupData(
        -1, None, None, None, None, 0, None, None, None, None, None, None
    )
    """ Backup data """

    log_enabled: bool
    """ Flag for logging """

    visualize_enabled: bool
    """ Flag for logging """

    backup_generation: int
    """ Interval for backup in generations """

    def __init__(
        self,
        problem: CGPProblem,
        backup_folder: str = None,
        population: CGPPopulation = None,
        log_enabled: bool = False,
        visualize_enabled: bool = False,
        backup_generation: int = BACKUP_GENERATION,
    ) -> None:
        """Initializes the executor

        @arg problem: Problem to solve
        @arg backup_folder: Backup folder. If not supplied a new folder labeled by date and time in 'bac' will be used.
        @arg population: Population to begin with. Defaults to None.
        @arg log_enabled: Flag for logging. Defaults to False.
        @arg visualize_enabled: Flag for visualization. Defaults to False.
        @arg backup_generation: Interval for backup in generations. Defaults to BACKUP_GENERATION.
        """
        self.problem = problem
        self.start_time = time.time()
        self.log_enabled = log_enabled
        self.visualize_enabled = visualize_enabled
        self.backup_generation = backup_generation

        self.backup_folder = ((backup_folder if backup_folder is not None else 'bac') +
                              '/' + problem.label + '_' + datetime.datetime.now().strftime('%Y%m%d%H%M%S') +
                              '_' +
                              generate_short_id(
            6))

        lambda_coef = (
            problem.strategy.lambda_coef
            if isinstance(problem.strategy, CGPStrategyOnePlusLambda)
            else NotImplementedError("Unknown strategy.")
        )

        self.population = (
            population
            if population is not None
            else CGPPopulation.random(self.problem, lambda_coef)
        )
        self.max_fitness = (
            self.problem.fitness_params.fn_coef
            * self.problem.tests_cnt
            * self.problem.outputs_cnt
            + self.problem.fitness_params.area_coef * self.problem.nodes_cnt
            + self.problem.fitness_params.time_coef * self.problem.columns_cnt
        ) if self.problem.fitness_params.fn_coef is not None else (
            self.problem.tests_cnt
            * self.problem.outputs_cnt
            + self.problem.fitness_params.area_coef * self.problem.nodes_cnt
            + self.problem.fitness_params.time_coef * self.problem.columns_cnt
        )
        self.backup_init()

    def backup_init(self) -> None:
        """Initializes the backup folder and files"""
        os.makedirs(self.backup_folder)
        print(f"Backup folder: {self.backup_folder}")
        self.problem.backup(f"{self.backup_folder}/{CONFIG_PATH}")

        with open(f"{self.backup_folder}/{CSV_PATH}", "w", newline="") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=CSV_FIELDNAMES)
            writer.writeheader()

    def backup(self) -> None:
        """Writes a line to the log CSV file"""

        fitness = self.population.best_individual.fitness
        self.backup_data = CGPExecutorBackupData(
            gen_time=(time.time() - self.backup_data.time - self.start_time)
            / (self.generation - self.backup_data.generation)
            * 1000,
            nc_gens=self.population.no_change_gens_cnt,
            fitness_evals=self.population.fitness_evaluation_cnt,
            graph_evals=self.population.graph_evaluations_cnt,
            node_evals=self.population.node_evaluations_cnt,
            generation=self.generation,
            fitness=fitness.total_fitness,
            functionality=fitness.functionality,
            active_nodes=fitness.active_nodes,
            longest_path=fitness.longest_path,
            time=time.time() - self.start_time,
            genotype=np.array2string(self.population.best_individual.genotype),
        )

        with open(f"{self.backup_folder}/{CSV_PATH}", "a", newline="") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=CSV_FIELDNAMES)
            writer.writerow(asdict(self.backup_data))

    @ classmethod
    def restore(
        cls,
        file_name: str,
        load_gen: int = -1,
        backup_folder: str = None,
        log_enabled: bool = False,
        visualize_enabled: bool = False,
        backup_generation: int = BACKUP_GENERATION,
    ) -> "CGPExecutor":
        """Restores the executor from a file or folder

        @arg file_name: YAML file or folder to restore from.
        @arg load_gen: If defined loads the individual from the n-th generation. Defaults to -1 (load last).
        @arg backup_folder: Backup folder.
        @arg log_enabled: Flag for logging. Defaults to False.
        @arg visualize_enabled: Flag for visualization. Defaults to False.
        @arg backup_generation: Interval for backup in generations. Defaults to BACKUP_GENERATION.

        @returns: New executor based on the stored one
        """
        problem = CGPProblem.restore(file_name if file_name.endswith(
            CONFIG_PATH) else f"{file_name}/{CONFIG_PATH}")

        executor: CGPExecutor = None
        if load_gen is not None:
            gen_line = None
            with open(f"{file_name[:-len(CONFIG_PATH)]}/{CSV_PATH}" if file_name.endswith(CONFIG_PATH) else f"{file_name}/{CSV_PATH}", "r", newline="") as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    if load_gen == -1:
                        gen_line = row
                    elif int(row["generation"]) == load_gen:
                        gen_line = row
                        break

            best_individual = CGPIndividual(
                problem,
                np.fromstring(
                    gen_line["genotype"].strip("[]"), dtype=np.int16, sep=" "
                ),
            )
            best_individual.execute()

            lambda_coef = (
                problem.strategy.lambda_coef
                if isinstance(problem.strategy, CGPStrategyOnePlusLambda)
                else NotImplementedError("Unknown strategy.")
            )

            executor = CGPExecutor(
                problem,
                population=CGPPopulation(
                    problem,
                    lambda_coef,
                    best_individual,
                ),
                backup_folder=backup_folder,
                log_enabled=log_enabled,
                visualize_enabled=visualize_enabled,
                backup_generation=backup_generation,
            )
            executor.generation = int(gen_line["generation"])
        else:
            executor = CGPExecutor(problem,
                                   log_enabled=log_enabled,
                                   visualize_enabled=visualize_enabled,
                                   backup_generation=backup_generation,)
        return executor

    def execute(self):
        """Executes the CGP algorithm

        @raises NotImplementedError: If unknown mutation strategy is used
        """
        self.state = CGPExecutorState.RUNNING

        if isinstance(self.problem.strategy, CGPStrategyOnePlusLambda):
            if isinstance(self.problem.mutation, CGPMutationConstantMutationRate):
                while not self.check_conditions():
                    # While criteria not met
                    self.population.breed(self.problem.mutation.mutation_rate)
            elif isinstance(self.problem.mutation, CGPMutationSimulatedAnnealing):
                temperature = 1.0
                while not self.check_conditions():
                    # While criteria not met
                    self.population.breed(
                        self.problem.mutation.start_mutation_rate * temperature
                    )
                    temperature = temperature * self.problem.mutation.cooling_rate
            elif isinstance(self.problem.mutation, CGPMutationGoldman):
                while not self.check_conditions():
                    # While criteria not met
                    self.population.breed()
            else:
                raise NotImplementedError

        self.state = CGPExecutorState.IDLE

    def handle_command(self, input_str: str):
        """Processes the input from the keyboard from a separate thread

        @arg input_str: Entered text
        """
        input = input_str.split(" ")
        if input[0] == "capture":
            self.state = CGPExecutorState.CAPTURING
            self.capturing_args = input[1:]
            sys.stdout.write("Capturing...\r\n")
            sys.stdout.flush()
            return
        elif input[0] == "exit":
            self.state = CGPExecutorState.STOPPING
            sys.stdout.write("Stopping...\r\n")
            sys.stdout.flush()
            return
        elif input[0] != "help":
            sys.stdout.write("Unknown input!\r\n")
        sys.stdout.write(
            """
Available commands:
capture [<parameters>]   Capture the best individual (capture --help for help)
exit                     Stop the execution
help                     Display this help message
"""
        )
        sys.stdout.flush()

    generation = 0
    """ Generation counter """

    def print_info(self):
        """Prints the information about the executor"""
        sys.stdout.write(
            "[%-20s] %d%%"
            % (
                "=" * int((self.backup_data.fitness / self.max_fitness) * 20),
                int((self.backup_data.fitness / self.max_fitness) * 100),
            )
        )
        sys.stdout.write(" | ")
        sys.stdout.write("Fitness: %.2f" % self.backup_data.fitness)
        sys.stdout.write(" | ")
        sys.stdout.write(
            "Functionality: %.2f%%" % (self.backup_data.functionality * 100)
        )
        sys.stdout.write(" | ")
        sys.stdout.write("Active nodes: %d" % self.backup_data.active_nodes)
        sys.stdout.write(" | ")
        sys.stdout.write("Longest path: %d" % self.backup_data.longest_path)
        sys.stdout.write(" | ")
        sys.stdout.write("Gen: %d" % self.backup_data.generation)
        sys.stdout.write(" | ")
        sys.stdout.write("Time: %.1fs " % self.backup_data.time)
        sys.stdout.write("(%.1fms/gen)" % (self.backup_data.gen_time))
        sys.stdout.write(" | ")
        sys.stdout.write("\r\n")
        sys.stdout.flush()

    first_full_functionality = True
    """ Flag for the first full functionality """

    def check_conditions(self) -> bool:
        """Checks the stopping conditions

        @returns: Continue calculation
        """
        done = (
            self.problem.stop_conditions.active_nodes is not None
            or self.problem.stop_conditions.longest_path is not None
        )
        if done and self.problem.stop_conditions.functionality is not None:
            done = (
                self.population.best_individual.fitness.functionality
                >= self.problem.stop_conditions.functionality
            )

        if done and self.problem.stop_conditions.active_nodes is not None:
            done = (
                self.population.best_individual.fitness.active_nodes
                <= self.problem.stop_conditions.active_nodes
            )

        if done and self.problem.stop_conditions.longest_path is not None:
            done = (
                self.population.best_individual.fitness.longest_path
                <= self.problem.stop_conditions.longest_path
            )

        if self.problem.stop_conditions.generations is not None:
            """ Stop after n generations - overrides other conditions """
            done = done or self.generation >= self.problem.stop_conditions.generations

        if (
            self.state == CGPExecutorState.CAPTURING
            or self.state == CGPExecutorState.STOPPING
            or done
        ):
            self.backup()
            if self.log_enabled:
                self.print_info()
            if self.visualize_enabled:
                self.visualize(self.capturing_args)
        elif (
            self.first_full_functionality
            and self.population.best_individual.fitness.functionality == 1.0
        ):
            self.backup()
            if self.log_enabled:
                self.print_info()
            if self.visualize_enabled:
                self.visualize(self.capturing_args)
            self.first_full_functionality = False
        elif self.generation % self.backup_generation == 0:
            self.backup()
            if self.log_enabled:
                self.print_info()
                # self.visualize(["--hide-inactive"])

        self.generation += 1

        if self.state == CGPExecutorState.STOPPING:
            sys.stdout.write("Stopped.\r\n")
            sys.stdout.flush()
            return True

        self.state = CGPExecutorState.RUNNING
        return done

    def visualize(self, args: list[str] = []):
        """Visualize the best individual

        @arg args: Arguments for the visualization. Defaults to [].
        """
        CGPVisualizer.visualize_args(
            self.population.best_individual,
            self.backup_folder + "/gen_" + str(self.generation) + ".pdf",
            args,
        )
