""" Provides the visualization of the CGP individual """

import tempfile
from .cgp_gate import CGPGate
from .cgp_individual import CGPIndividual
from .cgp_problem import CGPProblem


class CGPVisualizer(object):
    """Provides the visualization of the CGP individual"""

    @staticmethod
    def visualize_args(individual: CGPIndividual, filename: str, args: list[str] = []):
        """Visualizes the individual with the given arguments

        @arg individual: Individual to be visualized
        @arg filename: Filename of the output PDF
        @arg args: _description_. Defaults to [].
        """
        if "--help" in args:
            print(
                """
Usage: capture [<parameters>]

Parameters for visualization:
  --view            Include this parameter to open the PDF after creation.
  --hide-inactive   Include this parameter to hide inactive nodes in the visualization.
  --ignore-rank     Include this parameter to ignore node ranking in the visualization.
  --splines <type>  Specify the type of splines used for edges in the visualization. Available types: polyline (default), ortho, curved, spline. For more information, visit: https://graphviz.org/docs/attrs/splines/.
  --store-gv        Include this parameter to store the Graphviz DOT file used for visualization.
  --snap-grid       Include this parameter to force grid pattern.
  --hide-inactive-edges Include this parameter to hide only inactive edges, but to keep the inactive nodes, in the visualization.
  --console         Output the genotype to the console instead of creating the visualization.
  --help            Display this help message.
"""
            )
            return

        if "--console" in args:
            print(individual.genotype)
            return

        splines = "polyline"
        if "--splines" in args:
            splines_index = args.index("--splines") + 1
            if splines_index < len(args):
                splines = args[splines_index]
            else:
                splines = "true"
        CGPVisualizer.visualize(
            individual,
            filename,
            view="--view" in args,
            hide_inactive="--hide-inactive" in args,
            ignore_rank="--ignore-rank" in args,
            splines=splines,
            store_gv="--store-gv" in args,
            snap_grid="--snap-grid" in args,
            hide_inactive_edges="--hide-inactive-edges" in args,
        )

    @staticmethod
    def get_node_label(
        problem: CGPProblem, gate: CGPGate, node_used: bool, node_label: str
    ) -> str:
        """Builds the html table that is used to display the logic gate


        @arg problem: Solved problem
        @arg gate: Gate to be displayed
        @arg node_used: True if the node is active in the individual
        @arg node_label: Label to be displayed in the node

        @returns: HTML table with the gate
        """
        lines = []
        lines.append(
            f"<tr><td port='in0'></td><td><font point-size='8'>0</font></td><td rowspan='{problem.gates_arity}'>{gate.label}<br/><font point-size='8'>{node_label}</font></td><td port='out' rowspan='{problem.gates_arity}'></td></tr>"
        )  # First line of the table - first input and the right side of the gate
        for i in range(1, problem.gates_arity):
            lines.append(
                f"<tr><td port='in{i}'></td><td><font point-size='8'>{i}</font></td></tr>"
            )  # Other inputs
        table = f"<<font color='{'black' if node_used else 'grey70'}'><table border='1' cellborder='0' cellspacing='0'>{''.join(lines)}</table></font>>"
        return table

    @staticmethod
    def visualize(
        individual: CGPIndividual,
        filename: str,
        view: bool = False,
        hide_inactive: bool = False,
        ignore_rank: bool = False,
        splines="polyline",
        store_gv: bool=False,
        snap_grid: bool=False,
        hide_inactive_edges: bool=False
    ) -> None:
        """Generates PDF with the visualization of the individual

        @arg individual: Individual to be visualized
        @arg filename: Filename of the output PDF
        @arg view: Open PDF after creation. Defaults to False.
        @arg hide_inactive: Hide inactive nodes. Defaults to False.
        @arg ignore_rank: Ignore rank of the nodes. Defaults to False.
        @arg splines: Which spline to use. Graphviz documantation: https://graphviz.org/docs/attrs/splines/. Defaults to "true".
        @arg store_gv: Store the .gv file. Defaults to False.
        @arg snap_grid: Snap nodes to grid.
        @arg hide_inactive_edges: Hide inactive edges.
        """
        try:
            from graphviz import Digraph
        except ImportError:
            print("Warning: Graphviz is not installed. Visualization is not available.")
            return

        graph = Digraph(
            graph_attr={
                "rankdir": "LR",
                "newrank": "true",
                "concentrate": "true" if hide_inactive else "false",
                "splines": splines,
                "nodesep": "0.25",
                "ranksep": "0.75",
            }
        )  # Create the graph, concentrate can't be true if inactive edges are not hidden as they would be concentrated with active ones, nodesep and ranksep are set to make the graph more clear

        invisible = Digraph(
            edge_attr={"style": "invis"}
        )  # Invisible nodes and edges to keep the structure of the graph

        # Inputs
        inputs = Digraph(graph_attr={"rank": "same"})
        inputs.node(
            "rank_input", style="invis"
        )  # Invisible node to keep inputs in line
        for i in range(individual.problem.inputs_cnt):
            inputs.node(f"input_{i}", label=f"<x<sub>{i}</sub>>", shape="cds")
        graph.subgraph(inputs)

        # Nodes
        for i in range(individual.problem.columns_cnt):
            column = Digraph(graph_attr={"rank": "same"} if not ignore_rank else {})
            column.node(
                f"rank_{i}", style="invis"
            )  # Invisible node to keep columns in line
            for j in range(individual.problem.rows_cnt):
                node = i * individual.problem.rows_cnt + j
                if not hide_inactive or individual.node_active(node):
                    column.node(f"node_{node}")
            graph.subgraph(column)

            # Invisible edges to keep nodes in columns
            if i == 0:
                invisible.edge(f"rank_input", "rank_0")
            else:
                invisible.edge(f"rank_{i-1}", f"rank_{i}")

        # Outputs
        outputs = Digraph(graph_attr={"rank": "same"})
        outputs.node(
            "rank_output", style="invis"
        )  # Invisible node to keep outputs in line
        for i in range(individual.problem.outputs_cnt):
            outputs.node(f"output_{i}", label=f"<Y<sub>{i}</sub>>", shape="cds")
        graph.subgraph(outputs)

        invisible.edge(
            f"rank_{individual.problem.columns_cnt-1}", "rank_output"
        )  # Invisible edge to keep outputs in line

        active_edges = Digraph(
            edge_attr={
                "arrowhead": "normal",
                "style": "solid",
                "color": "black",
                "arrowsize": "0.5",
            }
        )
        inactive_edges = Digraph(
            edge_attr={
                "arrowhead": "normal",
                "style": "dotted",
                "color": "grey70",
                "arrowsize": "0.5",
            }
        )

        ## Read the genotype and create the graph
        for i in range(individual.problem.columns_cnt):
            for j in range(individual.problem.rows_cnt):
                node = i * individual.problem.rows_cnt + j
                gene_index = node * (individual.problem.gates_arity + 1)
                fn_index = individual.genotype[gene_index]
                node_used = True if individual.node_active(node) else False
                if hide_inactive and not node_used:
                    continue
                graph.node(
                    f"node_{node}",
                    label=CGPVisualizer.get_node_label(
                        individual.problem,
                        individual.problem.gates[fn_index],
                        node_used,
                        f"c<sub>{i},{j}</sub>",
                    ),
                    shape="plain",
                    color="black" if node_used else "grey70",
                    style="solid" if node_used else "dashed",
                )

                # Edges between nodes
                for k in range(1, individual.problem.gates_arity + 1):
                    conn_gene_index = gene_index + k
                    prev_node = (
                        individual.genotype[conn_gene_index]
                        - individual.problem.inputs_cnt
                    )
                    edge_used = (
                        node_used and k <= individual.problem.gates[fn_index].arity
                    )

                    if hide_inactive and not edge_used:
                        continue
                    if prev_node >= 0:
                        if edge_used:
                            active_edges.edge(
                                f"node_{prev_node}",
                                f"node_{node}",
                                headport=f"in{k-1}",
                                tailport="e",
                            )
                        else:
                            inactive_edges.edge(
                                f"node_{prev_node}",
                                f"node_{node}",
                                headport=f"in{k-1}",
                                tailport="e",
                            )
                    else:
                        input_node = prev_node + individual.problem.inputs_cnt
                        if edge_used:
                            active_edges.edge(
                                f"input_{input_node}",
                                f"node_{node}",
                                headport=f"in{k-1}",
                                tailport="e",
                            )
                        else:
                            inactive_edges.edge(
                                f"input_{input_node}",
                                f"node_{node}",
                                headport=f"in{k-1}",
                                tailport="e",
                            )

        # Edges from nodes to outputs
        for i in range(individual.problem.outputs_cnt):
            gene_index = individual.problem.nodes_genes_cnt + i
            prev_node = individual.genotype[gene_index] - individual.problem.inputs_cnt
            if prev_node >= 0:
                active_edges.edge(
                    f"node_{prev_node}",
                    f"output_{i}",
                    headport="w",
                    tailport="e",
                )
            else:
                input_node = prev_node + individual.problem.inputs_cnt
                active_edges.edge(
                    f"input_{input_node}",
                    f"output_{i}",
                    headport="w",
                    tailport="e",
                    arrowsize="0.5",
                )

        if not ignore_rank:
            graph.subgraph(invisible)
        graph.subgraph(active_edges)
        if not hide_inactive_edges:
            graph.subgraph(inactive_edges)

        if snap_grid:
            grid_eges = Digraph( edge_attr={
                "style": "invis"
            })
            for i in range(individual.problem.inputs_cnt):
                for j in range(individual.problem.rows_cnt):
                    for k in range(individual.problem.gates_arity):
                        grid_eges.edge(
                            f"input_{i}",
                            f"node_{j}",
                            headport=f"in{k}",
                            tailport="e",
                        )
            for column in range(individual.problem.columns_cnt-1):
                for i in range(individual.problem.rows_cnt):
                    for j in range(individual.problem.rows_cnt):
                        for k in range(individual.problem.gates_arity):
                            grid_eges.edge(
                                f"node_{i + column*individual.problem.rows_cnt}",
                                f"node_{j + (column+1)*individual.problem.rows_cnt}",
                                headport=f"in{k}",
                                tailport="e",
                            )
            for i in range(individual.problem.rows_cnt):
                for j in range(individual.problem.outputs_cnt):
                    grid_eges.edge(
                        f"node_{i + (individual.problem.columns_cnt-1)*individual.problem.rows_cnt}",
                        f"output_{j}",
                        headport="w",
                        tailport="e",
                    )
            graph.subgraph(grid_eges)

        graph.render(
            filename=filename + ".gv" if store_gv else tempfile.mktemp(".gv"),
            outfile=filename,
            view=view,
            format="pdf",
        )

        print(f"Visualization saved to {filename}")
