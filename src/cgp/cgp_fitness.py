"""CGPIndividualFitness class definition"""

from types import NoneType
import numpy as np


class CGPIndividualFitness:
    """CGPIndividual detailed fitness class"""

    fn_val: float = 0.0
    """Functionality part of the fitness value"""
    area_val: float
    """Area part of the fitness value"""
    time_val: float
    """Time part of the fitness value"""
    fn_reward: float = 0.0
    """Functionality reward part of the fitness value"""

    functionality: float = 0.0
    """Functionality value between 0 and 1 - 1 is best"""
    active_nodes: np.int16
    """Number of active nodes"""
    longest_path: np.int16
    """Longest path in the graph"""
    total_fitness: float
    """Total fitness value - the bigger the better"""

    def __init__(
        self,
        active_nodes: np.int16,
        max_active_nodes: np.int16,
        area_coef: float,
        longest_path: np.int16,
        max_longest_path: np.int16,
        time_coef: float,
    ):
        """Initializes the fitness value

        @arg active_nodes: Number of active nodes
        @arg max_active_nodes: Maximum number of active nodes
        @arg area_coef: Area coefficient
        @arg longest_path: Longest path in the graph
        @arg max_longest_path: Maximum longest path in the graph
        @arg time_coef: Time coefficient
        """
        self.active_nodes = active_nodes
        self.longest_path = longest_path

        self.area_val = area_coef * (max_active_nodes - active_nodes)
        self.time_val = time_coef * (max_longest_path - longest_path)

    def set_functionality(
        self,
        correct_bits: np.int16,
        max_correct_bits: np.int16,
        fn_coef: np.float32 | NoneType,
        fn_reward: np.float32 | NoneType,
    ):
        """Sets functionality value

        @arg correct_bits: Number of correct bits
        @arg max_correct_bits: Maximum number of correct bits
        @arg fn_coef: Functionality coefficient
        @arg fn_reward: Functionality reward
        """
        self.functionality = correct_bits / max_correct_bits

        if fn_coef is not None:
            """ One-stage fitness function """
            self.fn_val = fn_coef * correct_bits

            if fn_reward is not None:
                self.fn_reward = fn_reward if correct_bits == max_correct_bits else 0.0
            else:
                self.fn_reward = 0.0

            self.total_fitness = self.fn_val + self.area_val + self.time_val + self.fn_reward
        else:
            """ Two-stage fitness function """
            self.fn_val = correct_bits
            if correct_bits == max_correct_bits:
                self.total_fitness = self.fn_val + self.area_val + self.time_val
            else:
                self.total_fitness = self.fn_val
