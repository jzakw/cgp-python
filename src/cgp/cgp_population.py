""" CGPPopulation class definition """
import multiprocessing
from types import NoneType
from .cgp_individual import CGPIndividual
from .cgp_problem import CGPProblem


SINGLE_CORE = True
"""Use single core for breeding"""


def breed_chunk(
    chunk_len: int, best_individual: CGPIndividual, mutation_rate: float | NoneType
) -> list[CGPIndividual]:
    """Copy best individual and mutate it chunk_len times

    @arg chunk_len: Number of individuals to mutate
    @arg best_individual: Parent individual
    @arg mutation_rate: Mutation rate

    @return: Children individuals
    """
    chunk = [best_individual.mutate(mutation_rate) for _ in range(chunk_len)]
    return chunk


class CGPPopulation:
    """Population of CGP individuals"""

    problem: CGPProblem
    """Problem to solve"""
    best_individual: CGPIndividual
    """Best individual in the population"""
    lambda_coef: int = 0
    """Number of individuals in the population (without the best one)"""

    chunks_len: list[int] = []
    """Number of individuals in each chunk"""

    no_change_gens_cnt: int = 0
    """Number of generations without best individual change"""
    fitness_evaluation_cnt: int = 0
    """Number of calls to evaluate fitness value - not counting individuals without change in active genes"""
    graph_evaluations_cnt: int = 0
    """Number of calls to evaluate graph"""
    node_evaluations_cnt: int = 0
    """Number of calls to evaluate node"""

    def __init__(
        self,
        problem: CGPProblem,
        lambda_coef: int,
        base_individual: CGPIndividual,
        cores: int = 1 if SINGLE_CORE else multiprocessing.cpu_count(),
    ):
        """Initializes the population

        @arg problem: Problem to solve
        @arg lambda_coef: Number of individuals in the population (without the best one)
        @arg base_individual: Parent to start the population with
        @arg cores: Number of tasks to be solved in parallel
        """
        self.problem = problem
        self.best_individual = base_individual
        self.lambda_coef = lambda_coef

        chunk_size = lambda_coef // cores
        self.chunks_len = [chunk_size for _ in range(cores)]
        self.chunks_len[-1] += lambda_coef % cores

    @classmethod
    def random(cls, problem: CGPProblem, lambda_coef: int) -> "CGPPopulation":
        """Initialize the population without a parent

        @arg problem: Problem to solve
        @arg lambda_coef: Number of individuals in the population (without the best one)

        @return: New population
        """
        base_individual = CGPIndividual.random(problem)
        population = cls(problem, lambda_coef, base_individual)
        # Uncomment for startup boost tests
        # for _ in range(4096): # Change the number according to the test
        #     population.breed(1.0)
        return population

    def breed(self, mutation_rate: float | NoneType = None):
        """Breed new generation

        @arg mutation_rate: Mutation rate
        """
        new_individuals: list[list[CGPIndividual]] = None

        if not SINGLE_CORE:
            with multiprocessing.Pool() as pool:
                new_individuals = pool.starmap(
                    breed_chunk,
                    [
                        (chunk_len, self.best_individual, mutation_rate)
                        for chunk_len in self.chunks_len
                    ],
                )
                pool.close()
                pool.join()
        else:
            new_individuals = [
                breed_chunk(chunk_len, self.best_individual, mutation_rate)
                for chunk_len in self.chunks_len
            ]

        no_change = True
        for chunk in new_individuals:
            new_fittest = max(chunk, key=lambda x: x.total_fitness())
            # For biased selection change >= to >
            if new_fittest.total_fitness() >= self.best_individual.total_fitness():
                self.best_individual = new_fittest
                no_change = False

            for individual in chunk:
                """Statistics update"""
                self.graph_evaluations_cnt += individual.graph_executions_cnt
                self.node_evaluations_cnt += individual.graph_executions_cnt * individual.nodes_used_cnt
                if individual.active_gene_changed:
                    self.fitness_evaluation_cnt += 1

        self.no_change_gens_cnt += 1 if no_change else 0
