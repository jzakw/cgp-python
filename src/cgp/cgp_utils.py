"""Utility functions for CGP"""

import numpy as np
import random
import string

INT8_MAX = np.iinfo(np.int8).max
"""Maximum value for int8"""
INT16_MAX = np.iinfo(np.int16).max
"""Maximum value for int16"""


def generate_short_id(length=6) -> str:
    """Generate a random short ID

    @arg length: Length of the ID

    @return: Random short ID
    """
    characters = string.ascii_letters + string.digits
    short_id = ''.join(random.choices(characters, k=length))
    return short_id
