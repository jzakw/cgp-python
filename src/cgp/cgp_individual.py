""" CGPIndividual class definition """

from types import NoneType
import numpy as np
import numpy.typing as npt
from .cgp_gate import CGPGate
from .cgp_fitness import CGPIndividualFitness
from .cgp_problem import CGPProblem


class CGPIndividual:
    """CGPIndividual class definition"""

    problem: CGPProblem
    """ Problem to solve """

    fitness: CGPIndividualFitness
    """ Detailed fitness """
    correct_outputs: np.int16 = 0
    """ Sum of correct outputs for all tests """
    genotype: npt.NDArray[np.int16] = None
    """ 
    Genotype of the individual - first (rows_cnt * columns_cnt * (arity + 1)) genes are node genes, rest are output genes

    The values of the genes are:
        - for node genes: 0 to gates_cnt - 1 for function genes (first node gene) and 0 to inputs_cnt + nodes_cnt - 1 for connection genes
        - for output genes: 0 to inputs_cnt + nodes_cnt - 1 (only connection genes are used for output genes)

    The value of connection genes is:
        - 0 to inputs_cnt - 1 for inputs
        - inputs_cnt to inputs_cnt + nodes_cnt - 1 for nodes

    Nodes are indexed from 0 to nodes_cnt - 1 based on the order of the genes.
    """

    nodes_used: npt.NDArray[np.int16] = np.empty(0, dtype=np.int16)
    """ Nodes that are used - list like array that contains indexes of nodes that are active """
    nodes_used_cnt: np.int16 = 0
    """ Number of nodes that are used (nodes_used used size) """

    graph_executions_cnt: np.int32 = 0
    """ Number of graph executions needed to get the functionality fitness """

    longest_path: np.int16 = 0
    """ Longest path in the graph """

    parent: "CGPIndividual" = None
    """ Parent individual """

    active_gene_changed: bool = False
    """ If active gene was changed """

    def __init__(
        self,
        problem: CGPProblem,
        genotype: npt.NDArray[np.int16],
        parent: "CGPIndividual" = None,
    ):
        """Initialize individual

        @arg problem: Problem to solve
        @arg genotype: Individual genotype
        @arg parent: Parent individual
        """
        self.problem = problem
        self.genotype = genotype

        self.parent = parent
        if parent is None:
            self.active_gene_changed = True

    @classmethod
    def random(cls, problem: CGPProblem) -> "CGPIndividual":
        """Initialize random individual

        @arg problem: Problem to solve

        @return: Random individual
        """
        genotype = np.empty(problem.genes_cnt,
                            dtype=np.int16)  # Create empty genotype

        individual = cls(problem, genotype)

        """Mutate all genes to get random individual"""
        for i in np.arange(problem.genes_cnt):
            individual.mutate_gene(i)

        individual.execute()

        return individual

    def mutate(self, mutation_rate: float | NoneType = None) -> "CGPIndividual":
        """Mutate individual

        @arg mutation_rate: Mutation rate. If None, Goldman mutation is used. The genes are mutated until an active gene is hit.

        @return: Mutated individual
        """
        cls = self.__class__

        mutant = cls(self.problem, self.genotype.copy(), parent=self)
        if mutation_rate is not None:
            mutations: np.int16 = np.ceil(
                self.problem.genes_cnt * mutation_rate
                # Count number of mutations as a percentage of all genes, rounded up (so always at least one mutation)
            )

            for _ in np.arange(mutations):
                """Mutate random gene"""
                mutant.mutate_gene(
                    np.random.randint(0, self.problem.genes_cnt)
                )  # Mutate random gene - index from 0 to genes count
        else:
            """ Goldman mutation """
            mutant.active_gene_changed = False  # Reset active gene changed

            while mutant.active_gene_changed is False:
                """Mutate random gene until an active gene is hit"""
                random_gene = np.random.randint(
                    0, self.problem.genes_cnt
                )  # Mutate random gene - index from 0 to genes count
                mutant.mutate_gene(random_gene)

        mutant.execute()

        mutant.parent = (
            None  # Remove parent reference - important for memory management
        )

        return mutant

    def mutate_gene(self, index: np.int16):
        """Mutate one gene and set active_gene_changed flag if the gene was active

        @arg index: Gene index
        """
        if index < self.problem.nodes_genes_cnt:
            """Node genes"""
            [node_index, gene_index_mod] = divmod(
                index, self.problem.gates_arity + 1)
            if gene_index_mod == 0:
                """Node Function gene"""
                random_gate = np.random.randint(
                    0, self.problem.gates_cnt
                )  # Get random number between 0 and gates count
                self.genotype[index] = random_gate
            else:
                """Node Connection gene"""
                nodes_left = (
                    node_index // self.problem.rows_cnt
                    # Number of nodes to the left = (index // rows) * rows
                ) * self.problem.rows_cnt

                if nodes_left > self.problem.levels_back_nodes:
                    # If there are more nodes on the left side than levels back limit, limit the connection to levels back
                    random_num = np.random.randint(
                        0, self.problem.levels_back_nodes + self.problem.inputs_cnt
                    )  # Get random number between 0 and levels back + inputs
                    new_gene = (
                        random_num  # Keep the random number if it is pointing to input
                        if random_num < self.problem.inputs_cnt
                        else random_num
                        + nodes_left
                        # Scale the gene match with the levels back if it is pointing to node
                        - self.problem.levels_back_nodes
                    )
                    self.genotype[index] = new_gene
                else:
                    # If there are less nodes on the left side than levels back limit, limit the connection to the left side
                    random_num = np.random.randint(
                        0, nodes_left + self.problem.inputs_cnt
                    )
                    # No need to scale the gene
                    self.genotype[index] = random_num
            self.active_gene_changed = self.active_gene_changed or (
                True
                if self.parent is None or self.parent.node_active(node_index)
                else False
            )
        else:
            """Output genes"""
            random_num = np.random.randint(
                0, self.problem.nodes_cnt + self.problem.inputs_cnt
            )  # Output genes can be connected to any node or input
            self.genotype[index] = random_num
            self.active_gene_changed = True  # Output genes are always active

    def execute(self):
        """
        Execute and get the functionality fitness
        """
        if self.active_gene_changed or self.parent is None:
            """If active gene was changed, execute the graph"""
            self.active_gene_changed = True
            self.mark_nodes_to_evaluate()
            self.init_fitness()
            self.correct_outputs = 0  # Reset correct outputs counter
            for i in np.arange(0, self.problem.tests_cnt):
                """Apply the test inputs and verify the results"""
                output = self.execute_graph(self.problem.test_inputs[i, :])
                correct_outputs = self.verify_result(
                    self.problem.test_outputs[i, :], output
                )
                self.correct_outputs += correct_outputs

                if self.parent is not None and i < (self.problem.tests_cnt - 1):
                    """Test if it the individual might have better functionality than the parent"""
                    max_correct_outputs = (
                        self.problem.tests_cnt - 1 - i
                        # Calculate the maximum number of correct outputs that the individual can have in the remaining tests
                    ) * self.problem.outputs_cnt + self.correct_outputs
                    # Calculate the functionality fitness based on the maximum number of correct outputs
                    self.calc_fitness(max_correct_outputs)
                    if self.parent.total_fitness() > self.total_fitness():
                        """If the parent has better functionality, stop the execution"""
                        break
            # Calculate the functionality fitness based on the actual number of correct outputs, this value will be wrong if the execution was stopped, but it is not important as the individual will not be used
            self.calc_fitness(self.correct_outputs)
        else:
            """If active gene was not changed, use the parent values"""
            self.nodes_used = self.parent.nodes_used
            self.nodes_used_cnt = self.parent.nodes_used_cnt
            self.longest_path = self.parent.longest_path
            self.correct_outputs = self.parent.correct_outputs
            self.fitness = self.parent.fitness

    def mark_nodes_to_evaluate(self):
        """Mark nodes that are used in the graph and get the longest path"""

        path_to: npt.NDArray[np.int16] = np.zeros(
            self.problem.nodes_cnt, dtype=np.int16
        )  # Length of the path to the nodes from output

        self.nodes_used = np.empty(
            self.problem.nodes_cnt, dtype=np.int16
        )  # Nodes that are active
        self.nodes_used_cnt = 0

        """Mark nodes that are directly connected to outputs as active"""
        for i in np.arange(0, self.problem.outputs_cnt):
            output_gene = self.genotype[self.problem.nodes_genes_cnt + i]
            node_to_evaluate = output_gene - self.problem.inputs_cnt
            if node_to_evaluate >= 0:
                path_to[node_to_evaluate] = 1

        """List through the nodes (from the end) and mark the longest path"""
        for i in np.arange(self.problem.nodes_cnt - 1, -1, -1):
            if path_to[i] != 0:
                """If the path to the node is not 0, mark the node active and update the path to the nodes that are connected to the node"""
                gene_index = i * (self.problem.gates_arity + 1)
                gate: CGPGate = self.problem.gates[self.genotype[gene_index]]

                """Iterate through the inputs of the gate"""
                for j in np.arange(
                    1,
                    min(
                        self.problem.gates_arity,
                        gate.arity,
                    )  # Use the gate arity if it is smaller than the problem gates arity
                    + 1,
                ):
                    node_to_evaluate = (
                        self.genotype[gene_index + j] -
                        self.problem.inputs_cnt  # Get the node index
                    )

                    """Update the path to the node if the new path is longer"""
                    if node_to_evaluate >= 0:  # If the node is not input
                        if path_to[node_to_evaluate] < path_to[i] + 1:
                            path_to[node_to_evaluate] = path_to[i] + 1

                # Mark the node as active
                self.nodes_used[self.nodes_used_cnt] = i
                self.nodes_used_cnt += 1  # Increase the number of active nodes

        self.longest_path = max(path_to)  # Get the longest path

    def execute_graph(self, input_value: npt.NDArray[np.int8]) -> npt.NDArray[np.int8]:
        """Execute the graph

        @arg input_value: Input value

        @return: Output value
        """
        # Load inputs
        nodes_output = np.empty(
            self.problem.inputs_cnt + self.problem.nodes_cnt, dtype=np.int8
            # Node output values padded with input values (inputs are at the begining), the index is corresponding to the connections gene value
        )
        # Load input values
        nodes_output[0:self.problem.inputs_cnt] = input_value

        """Iterate through the nodes (from the begining) and compute the output value"""
        for i in np.arange(self.nodes_used_cnt - 1, -1, -1, dtype=np.int16):
            node = self.nodes_used[i]
            gene_index = node * (self.problem.gates_arity + 1)
            # Compute node output
            fn_index = self.genotype[gene_index]
            fn_inputs: npt.NDArray[np.int8] = nodes_output[
                self.genotype[
                    gene_index + 1: gene_index + self.problem.gates_arity + 1
                ]
            ]

            # Get the output value from the precalcualted gates map
            nodes_output[node +
                         self.problem.inputs_cnt] = self.problem.gates_map[(fn_index, *fn_inputs,)]

        self.graph_executions_cnt += 1

        # Get outputs values
        graph_output = nodes_output[self.genotype[self.problem.nodes_genes_cnt:]]

        return graph_output

    def verify_result(
        self, expected_output: npt.NDArray[np.int8], actual_output: npt.NDArray[np.int8]
    ) -> np.int16:
        """Verify that output is correct

        @arg expected_output: Expected output
        @arg actual_output: Actual output

        @return: Number of correct outputs - 0 is worst
        """
        correct_bits: np.int16 = np.sum(actual_output == expected_output)

        return correct_bits

    def node_active(self, node_index: np.int16) -> bool:
        """Check if the node is active

        @arg node_index: Node index
        @return: True if the node is active
        """
        return node_index in (self.nodes_used[0: self.nodes_used_cnt])

    def init_fitness(self) -> CGPIndividualFitness:
        """Prepare individual fitness class (except functionality)"""

        self.fitness = CGPIndividualFitness(
            active_nodes=self.nodes_used_cnt,
            max_active_nodes=self.problem.nodes_cnt,
            area_coef=self.problem.fitness_params.area_coef,
            longest_path=self.longest_path,
            max_longest_path=self.problem.columns_cnt,
            time_coef=self.problem.fitness_params.time_coef,
        )

    def calc_fitness(self, correct_outputs: np.int16) -> CGPIndividualFitness:
        """Calculate final individual fitness class (functionality), based on precalculated values

        @arg correct_outputs: Number of correct outputs
        """
        self.fitness.set_functionality(
            correct_outputs,
            self.problem.tests_cnt * self.problem.outputs_cnt,
            self.problem.fitness_params.fn_coef,
            self.problem.fitness_params.fn_reward,
        )

    def total_fitness(self) -> float:
        """Get individual fitness

        @return: Fitness value
        """
        return self.fitness.total_fitness
