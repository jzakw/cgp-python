""" CGP gate definitions and logic functions """

from itertools import product
from typing import Callable
import numpy as np
import numpy.typing as npt

from .cgp_utils import INT16_MAX


class CGPGate:
    """Gate class"""

    fn: Callable[[npt.NDArray[np.int8]], np.int8]
    """ Gate function """
    label: str
    """ Gate label to be used in graphs and by which it is identified """
    arity: np.int16 = INT16_MAX
    """ Arity of the gate """

    fn_map: npt.NDArray[np.int8]
    """ Function output map for the gate """

    def __init__(
        self,
        fn: Callable[[npt.NDArray[np.int8]], np.int8],
        label: str,
        arity: np.int16 = INT16_MAX,
    ):
        """Initialize the gate

        @arg fn: Gate function
        @arg label: Gate label
        @arg arity: Arity of the gate
        """
        self.fn = fn
        self.label = label
        self.arity = arity

    def calculate_fn_map(self, arity: np.int8):
        """Calculate function map for the gate

        @arg arity: Arity of the gate
        """
        self.fn_map = np.empty(
            (2,) * arity, dtype=np.int8
        )  # Create n-dimensional array of 2s

        coordinates = np.array(
            list(product([0, 1], repeat=arity)), dtype=np.int8)

        for i in range(len(coordinates)):
            self.fn_map[tuple(coordinates[i])] = self.fn(coordinates[i])

    def get_output(self, inputs: npt.NDArray[np.int8]):
        """Get gate value

        @arg inputs: Gate inputs
        """
        return self.fn_map[tuple(inputs)]


class Logic(object):
    """Logic functions for CGP gates"""

    @staticmethod
    def and_fn(inputs):
        """AND function"""
        return 1 if np.prod(inputs) == 1 else 0

    @staticmethod
    def nand_fn(inputs):
        """NAND function"""
        return 1 if np.prod(inputs) == 0 else 0

    @staticmethod
    def or_fn(inputs):
        """OR function"""
        return 1 if np.sum(inputs) >= 1 else 0

    @staticmethod
    def nor_fn(inputs):
        """NOR function"""
        return 1 if np.sum(inputs) == 0 else 0

    @staticmethod
    def xor_fn(inputs):
        """XOR function"""
        return 1 if np.sum(inputs) % 2 == 1 else 0

    @staticmethod
    def xnor_fn(inputs):
        """XNOR function"""
        return 1 if np.sum(inputs) % 2 == 0 else 0

    @staticmethod
    def repeater_fn(inputs):
        """Repeater function (for input 0)"""
        return inputs[0]

    @staticmethod
    def not_fn(inputs):
        """NOT function (for input 0)"""
        return 0 if inputs[0] else 1


class CGPBasicGates(object):
    """Basic CGP gates"""

    and_gate = CGPGate(Logic.and_fn, "AND")
    """ AND gate """
    nand_gate = CGPGate(Logic.nand_fn, "NAND")
    """ NAND gate"""
    or_gate = CGPGate(Logic.or_fn, "OR")
    """ OR gate """
    nor_gate = CGPGate(Logic.nor_fn, "NOR")
    """ NOR gate """
    xor_gate = CGPGate(Logic.xor_fn, "XOR")
    """ XOR gate """
    xnor_gate = CGPGate(Logic.xnor_fn, "XNOR")
    """ XNOR gate """
    repeater_gate = CGPGate(Logic.repeater_fn, "REPEATER", 1)
    """ Repeater gate """
    not_gate = CGPGate(Logic.not_fn, "NOT", 1)
    """ NOT gate """

    Group1 = [nand_gate, or_gate, xor_gate]
    """ Group 1 gates """
    Group2 = [and_gate, nand_gate, or_gate, nor_gate, xor_gate, xnor_gate]
    """ Group 2 gates """
    GroupAll = [
        and_gate,
        nand_gate,
        or_gate,
        nor_gate,
        xor_gate,
        xnor_gate,
        repeater_gate,
        not_gate,
    ]
    """ All gates """
