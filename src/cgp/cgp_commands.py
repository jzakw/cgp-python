"""Module for handling commands in a separate thread."""

import threading
from typing import Callable

from .cgp_executor import CGPExecutor


class CGPCommands(object):
    """Class for handling commands in a separate thread."""

    executor: CGPExecutor = None
    """ CGPExecutor instance """

    exit_flag: bool = False
    """ Exit flag """

    keyboard_thread: "KeyboardThread" = None
    """ Keyboard thread """

    @staticmethod
    def init(headless=False) -> None:
        """Initialize the queue"""
        if not headless and CGPCommands.keyboard_thread is None:
            CGPCommands.keyboard_thread = KeyboardThread(
                CGPCommands.handle_command, CGPCommands.set_exit_flag
            )

    @staticmethod
    def destroy() -> None:
        """Destroy the queue"""
        if CGPCommands.keyboard_thread is not None:
            CGPCommands.keyboard_thread.stop()
            CGPCommands.keyboard_thread.join()
            CGPCommands.keyboard_thread = None

    @staticmethod
    def register_executor(executor: CGPExecutor) -> None:
        """Add a command to the queue"""
        CGPCommands.executor = executor

    @staticmethod
    def handle_command(command: str) -> None:
        """Handle command"""
        if CGPCommands.executor is not None:
            CGPCommands.executor.handle_command(command)
        else:
            print("No executor registered.")

    @staticmethod
    def set_exit_flag() -> None:
        """Set exit flag"""
        CGPCommands.exit_flag = True


class KeyboardThread(threading.Thread):
    """Class for keyboard input handling in a separate thread."""

    callback: Callable[[str], None]
    """ Callback function """

    exit_callback: Callable[[str], None]
    """ Exit callback function """

    stop_event: threading.Event
    """ Event to stop the thread """

    def __init__(
        self,
        callback: Callable[[str], None],
        exit_callback: Callable[[], None],
    ):
        """Initialize KeyboardThread

        @arg callback: Callback function
        """
        super(KeyboardThread, self).__init__()
        self.callback = callback
        self.stop_event = threading.Event()
        self.exit_callback = exit_callback
        self.start()

    def run(self):
        """Method to be invoked in a separate thread"""
        while True:
            user_input = input()
            if self.stop_event.is_set():
                break

            self.callback(user_input)

            if user_input.split(" ")[0] == "exit":
                self.exit_callback()
                break

    def stop(self):
        """Set stop internal flag"""
        self.stop_event.set()
