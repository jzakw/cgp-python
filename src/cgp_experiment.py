""" Module for running experiments """

from datetime import datetime
import sys
import time

from cgp.cgp_problem import CGPProblem
from cgp.cgp_executor import CGPExecutor


class Experiment():
    """ Class for running experiments """

    problems: list[CGPProblem]
    backup_folder: str
    test_count: int
    backup_generations: int

    def __init__(self, problems: list[CGPProblem], backup_folder: str, test_count: int, backup_generations: int = 100):
        """Constructor for Experiment class

        @arg problems: List of problems to run
        @arg backup_folder: Folder to save backups
        @arg test_count: Number of tests to run
        """
        self.problems = problems
        self.backup_folder = backup_folder
        self.test_count = test_count
        self.backup_generations = backup_generations

    def run(self) -> None:
        """Run experiments"""
        if "-h" in sys.argv:
            print(f"""Output folder: {self.backup_folder}""")
            print(f"""Test count: {self.test_count}""")
            print(f"""Backup generations: {self.backup_generations}""")
            print("Experiments:")
            for i, problem in enumerate(self.problems):
                print(f"""\t{i}: {problem.label}, max. gens {
                    problem.stop_conditions.generations}""")
            return
        if "-e" in sys.argv:
            experiment_index = sys.argv.index("-e") + 1
            if experiment_index < len(sys.argv):
                problem_index = sys.argv[experiment_index]
                self.run_experiment(int(problem_index))
        else:
            for i in range(len(self.problems)):
                print(f"""[{datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                            }] Experiment {i}/{len(self.problems)}""")
                self.run_experiment(i)

    def run_experiment(self, index: int) -> None:
        """Run a single experiment"""
        test_count = self.test_count
        start_time = time.time()
        print(f"""Experiment {self.problems[index].label}, max. gens {
            self.problems[index].stop_conditions.generations}""")
        if "-t" in sys.argv:
            test_index = sys.argv.index("-t") + 1
            if test_index < len(sys.argv):
                test_count = int(sys.argv[test_index])
        for i in range(test_count):
            print(f"""[{datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                        }] Experiment {index}, iteration {i}/{test_count}""")
            cgpExecutor = CGPExecutor(
                self.problems[index], backup_folder=self.backup_folder,
                backup_generation=self.backup_generations,
                log_enabled="-l" in sys.argv,
                visualize_enabled="-v" in sys.argv
            )
            cgpExecutor.execute()
            duration = time.time() - start_time
            print(f"""Done {i}. Estimated time left: {
                (duration / (i+1)) * (test_count-i-1) // 60} minutes""")


if __name__ == "__main__":
    raise Exception("This module is not supposed to be run directly!")
