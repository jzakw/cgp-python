""" Experiment with different grid sizes for 2-bit multiplicator. """

from cgp.cgp_problem import multiplicator_fn, CGPProblem, CGPProblemStopConditions, CGPProblemFitnessParams, CGPStrategyOnePlusLambda, CGPMutationGoldman, CGPBasicGates
from cgp_experiment import Experiment

BACKUP_FOLDER = "exp/mult2_mix"
TEST_COUNT = 50


def run() -> None:
    """ Run the experiment """
    problems: list[CGPProblem] = []

    generations = [
        [1000000, 350000, 300000],  # 4x4
        [500000, 200000, 100000, 180000],  # 5x5
        [500000, 150000, 150000, 200000, 300000],  # 6x6
        [750000, 250000, 200000, 300000, 400000, 700000],  # 7x7
    ]

    for i in range(4, 7+1):
        for j in range(1, i):
            problems.append(CGPProblem.auto_tests(
                label=f"Multiplicator2bit_{i}x{i}_levels_back_{j}_lambda_4",
                columns_cnt=i,
                rows_cnt=i,
                levels_back=j,
                inputs_cnt=4,
                outputs_cnt=4,
                gates_arity=2,
                gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                       CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                       CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
                problem_fn=lambda input: multiplicator_fn(input, 2, 2),
                strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
                mutation=CGPMutationGoldman(),
                fitness_params=CGPProblemFitnessParams(
                    fn_coef=5,
                    area_coef=1,
                    time_coef=1,
                    fn_reward=2,
                ),
                stop_conditions=CGPProblemStopConditions(
                    active_nodes=7, longest_path=2, generations=generations[i-4][j-1]),
            ))

    experiment = Experiment(problems, BACKUP_FOLDER, TEST_COUNT)
    experiment.run()


if __name__ == "__main__":
    run()
