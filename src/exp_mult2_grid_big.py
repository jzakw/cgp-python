""" Experiment with different grid sizes for 2-bit multiplicator. """

from cgp.cgp_problem import multiplicator_fn, CGPProblem, CGPProblemStopConditions, CGPProblemFitnessParams, CGPStrategyOnePlusLambda, CGPMutationGoldman, CGPBasicGates
from cgp_experiment import Experiment

BACKUP_FOLDER = "exp/mult2_big"
TEST_COUNT = 50


def run() -> None:
    """ Run the experiment """
    problems: list[CGPProblem] = []

    for i in range(8, 15+1):
        problems.append(CGPProblem.auto_tests(
            label=f"Multiplicator2bit_{i}x{i}_levels_back_{i-1}_lambda_4",
            columns_cnt=i,
            rows_cnt=i,
            levels_back=i-1,
            inputs_cnt=4,
            outputs_cnt=4,
            gates_arity=2,
            gates=[CGPBasicGates.and_gate, CGPBasicGates.or_gate,
                   CGPBasicGates.nand_gate, CGPBasicGates.nor_gate,
                   CGPBasicGates.xor_gate, CGPBasicGates.xnor_gate],
            problem_fn=lambda input: multiplicator_fn(input, 2, 2),
            strategy=CGPStrategyOnePlusLambda(lambda_coef=4),
            mutation=CGPMutationGoldman(),
            fitness_params=CGPProblemFitnessParams(
                fn_coef=5,
                area_coef=1,
                time_coef=1,
                fn_reward=2,
            ),
            stop_conditions=CGPProblemStopConditions(
                active_nodes=7, longest_path=2, generations=400000),
        ))

    experiment = Experiment(problems, BACKUP_FOLDER, TEST_COUNT)
    experiment.run()


if __name__ == "__main__":
    run()
